from pyqcm import *
import qcm_ED
import numpy as np
from subprocess import run


#-------------------------------------------------------------------------------
def export_to_wietek(n_sites, filename, clus=0, label=0):

    mix = mixing()
    if mix !=0 and mix != 4:
        print("Wietek's solver is restricted to the normal phase for the moment")
        exit()

    # writing the "latticefile"
    out = open(filename, 'w')
    out.write('[Interactions]\n')
    H = qcm_ED.hopping_matrix(False, label, True)
    N = len(H)
    for i in range(N):
        for j in range(i):
            if H[i,j] != 0: 
                out.write('HUBBARDHOP T{0:d}{1:d} {0:d} {1:d}\n'.format(i,j))
    for i in range(N):
        if H[i,i] != 0: 
            out.write('HUBBARDMU MU{0:d} {0:d}\n'.format(i))

    for i in range(n_sites):
        out.write('HUBBARDV V {0:d} {0:d}\n'.format(i))
    for i in range(n_sites):
        out.write('HUBBARDMU MUC {0:d}\n'.format(i))

    out.close()

    P = parameters(label)
    U = P['U']

    # writing the "couplingfile"
    out = open('couplingfile', 'w')
    out.write('[couplings]\nV {0:f}\n'.format(U/2))
    out.write('MUC {0:f}\n'.format(U/2))
    for i in range(N):
        for j in range(i):
            if H[i,j] != 0: 
                out.write('T{0:d}{1:d} {2:f}\n'.format(i,j,-H[i,j].real))
    for i in range(N):
        if H[i,i] != 0: 
            out.write('MU{0:d} {1:f}\n'.format(i,-H[i,i].real))
    out.close()

    # writing the "corrfile"
    d = qcm_ED.Green_function_dimension(clus)
    out = open('corrfile', 'w')
    for i in range(d):
        for j in range(d):
            out.write('{0:d}  {1:d}\n'.format(i,j))
    out.close()

#-------------------------------------------------------------------------------
def extract_NupNdw(sec):
    """
    extracts the values of N and S (quantum numbers) from the sector string sec.
    returns an array of 2-uples.
    """
    p1 = sec.find('N')
    p2 = sec.find(':S')
    N = int(sec[p1+1:p2])
    S = int(sec[p2+2:])
    Nup = (N+S)//2
    Ndw = N - Nup
    return Nup, Ndw



#-------------------------------------------------------------------------------
def wietek_solver(sec, cluster=0, niter=400, dyniter=400, label=0, cf = False):
    """
    :param str sec: sector string
    :param int niter: maximum number of iterations
    :param int label: label of the instance
    :param boolean cf: True if the continued fraction representation (ordinary Lanczos) is used. Otherwise band Lanczos.
    :param boolean gs: Only computes the ground state energy
    """

    latticefile = 'model.pbc'
    L, nband, clus, bath = model_size()
    n_sites = clus[cluster]

    if cf:
        algo = 'lanczos'
    else:
        algo = 'bandlanczos'
    stem = 'tmp_'+str(cluster+1)
    outfile = stem + '.out'


    export_to_wietek(n_sites, latticefile, clus=cluster, label=label)
    # extracting Nup and Ndow

    secs = sec.split('/')
    # first find the sector containing the ground state if more than one
    prec = 1e-14
    if(len(secs)>1):
        E0 = []
        for s in secs:
            Nup, Ndw = extract_NupNdw(s)
            command = [
                'mpirun', 
                '/mnt/home/awietek/Research/Software/hydra/bin/hubbarddynamicsmpi',
                '--outfile '+outfile,
                '--latticefile '+latticefile,
                '--corrfile corrfile',
                '--couplingfile couplingfile',
                '--nup {:d}'.format(Nup),
                '--ndown {:d}'.format(Ndw),
                '--algorithm '+algo,
                '--precision {:g}'.format(prec),
                '--dynprecision {:g}'.format(1e-12),
                '--iters {:d}'.format(niter),
                '--dyniters {:d}'.format(dyniter),
                '--verbosity 0',
                '--lobpcgbands 1'
            ]
            run(command)
            fin = open(outfile, 'r')
            line = fin.readline()
            pos = line.split(' ')
            E0.append(float(pos[-1]))
        I = np.argmin(np.array(E0))
    else:
        I = 0

    Nup, Ndw = extract_NupNdw(secs[I])
    command = [
        'mpirun', 
        '/mnt/home/awietek/Research/Software/hydra/bin/hubbarddynamicsmpi',
        '--outfile '+outfile,
        '--latticefile '+latticefile,
        '--corrfile corrfile',
        '--couplingfile couplingfile',
        '--nup {:d}'.format(Nup),
        '--ndown {:d}'.format(Ndw),
        '--algorithm '+algo,
        '--precision {:g}'.format(prec),
        '--dynprecision {:g}'.format(1e-12),
        '--iters {:d}'.format(niter),
        '--dyniters {:d}'.format(dyniter),
        '--verbosity 0',
        '--lobpcgbands 1'
    ]
    run(command)




    sol2 = ''
    if cf:
        for a in range(n_sites):
            for b in range(a+1):
                sol2 += 'irrep: 0  a: {:d} b: {:d}\n'.format(a, b)
                e0, weight, alphas, betas = read_e0_weight_alphas_betas(outfile, "cdagdn", a, b)
                sol2 += 'floors: {:d}\n'.format(len(alphas))
                nfloor = len(alphas)
                if a == b:
                    weight = 0.5
                sol2 += '{0:.20f} {1:.20f}\n'.format(alphas[0]-e0, weight)
                for i in range(1, nfloor):
                    sol2 += '{0:.20f} {1:.20f}\n'.format(alphas[i]-e0, betas[i-1]*betas[i-1])
                e0, weight, alphas, betas = read_e0_weight_alphas_betas(outfile, "cdn", a, b)
                sol2 += 'floors: {:d}\n'.format(len(alphas))
                nfloor = len(alphas)
                if a == b:
                    weight = 0.5
                sol2 += '{0:.20f} {1:.20f}\n'.format(-alphas[0]+e0, weight)
                for i in range(1, len(alphas)):
                    sol2 += '{0:.20f} {1:.20f}\n'.format(-alphas[i]+e0, betas[i-1]*betas[i-1])

    else:    
        wp, Qp, e0 = Lehmann_rep('cdagdn', outfile)
        wm, Qm, e0 = Lehmann_rep('cdn', outfile)
        w = np.concatenate([wm,wp])
        Q = np.concatenate([Qm.T,Qp.T])
        verif = np.dot(Q.conj().T, Q)
        print('norm check on the Q matrix: ', np.diag(verif))
        sol2 += 'w\t{:d}\t{:d}\n'.format(Q.shape[1],Q.shape[0])
        for i in range(Q.shape[0]):
            sol2 += '{:.20f}\t'.format(w[i])
            for j in range(Q.shape[1]):
                sol2 += '{:.20f}\t'.format(Q[i,j])
            sol2 += '\n'

    sol = ''
    P = parameters()
    for p in P:
        if p[-2:] == "_1":
            sol += p[:-2] + '\t{:.20f}\n'.format(P[p])
    sol += '\n\nGS_energy: {:.20f}\tGS_sector: {:s}:1\n'.format(e0, sec)
    if cf:
        sol += 'GF_format: cf\n'
    else:
        sol += 'GF_format: bl\n'
    sol += 'mixing	0\n'
    sol += 'state\n'
    sol += '{:s}\t{:.20f}\t1\n'.format(sec,e0)
    sol += sol2

    solfile = stem + '.sol'
    out = open(solfile, 'w')
    out.writelines(sol)
    out.close()
    read_cluster_model_instance(sol, label=label)




def Lehmann_rep(ftype, outfile):
    e0, sites, tmatrix, overlaps = read_e0_sites_tmatrix_overlaps(outfile, ftype)
    n_sites = len(sites)
    dim = tmatrix.shape[0]
    evals, evecs = np.linalg.eigh(tmatrix)
    Q = np.dot(overlaps.conj().T, evecs[0:n_sites, :])
    if 'dag' in ftype:
        evals = evals - e0
    else:
        evals = -(evals - e0)
    return evals, Q, e0



################################################################################
# functions below written by Alexander Wietek


def read_e0_weight_alphas_betas(filename, ftype, mu, nu):
    gsline = [ line for line in open(filename) if 'energy' in line][0]
    e0 = float(gsline.split(":")[1])
    
    lines = []

    # Parse the alphas, betas and the weight for mu, nu
    with open(filename) as fl:
        typestring="ftype: {}, s1: {}, s2: {}".format(ftype, mu, nu)

        active = False
        for line in fl:
            if typestring in line:
                active = True
            elif "ftype" in line:
                active = False
            
            if active:
                if "weight" in line:
                    weight = float(line.split(":")[1])
                lines.append(line)
    
    arr = np.loadtxt(lines)
    alphas = arr[:,0]
    betas= arr[:,1]
    return e0, weight, alphas, betas

def t_matrix(alphas, betas):
    return np.diag(alphas) + np.diag(betas[:-1], 1) + np.diag(betas[:-1], -1) 

def contfrac(z, alphas, betas, level):
    if (level==len(alphas)):
        return 0.0;
    elif level == 0:
        return 1./ (z - alphas[0] - contfrac(z, alphas, betas, 1));
    else:
        return (betas[level-1]*betas[level-1]) / \
            (z - alphas[level] - contfrac(z, alphas, betas, level+1));

def greens_function(zs, alphas, betas):
    return contfrac(zs, alphas, betas, 0)

def spectral_function(zs, alphas, betas):
    return -1./np.pi * np.imag(contfrac(zs, alphas, betas, 0))

def G_plus(omegas, eta, ftype, mu, nu, filename):
    e0, weight, alphas, betas = read_e0_weight_alphas_betas(filename, ftype, mu, nu)
    
    if "dag" in ftype:
        zs = omegas + e0 + 1j*eta 
        G_plus = weight*greens_function(zs, alphas, betas)
    else:
        zs =-omegas + e0 - 1j*eta
        G_plus = -weight*greens_function(zs, alphas, betas)
    return G_plus

def G_mu_nu(omegas, eta, ftype, mu, nu, filename):
    G_plus_mu_nu = G_plus(omegas, eta, ftype, mu, nu, filename)
    G_mu_mu = G_plus(omegas, eta, ftype, mu, mu, filename) / 4.
    G_nu_nu = G_plus(omegas, eta, ftype, nu, nu, filename) / 4.
    G_mu_nu = (G_plus_mu_nu - G_mu_mu - G_nu_nu) / 2.
    return G_mu_nu


def read_e0_sites_tmatrix_overlaps(filename, ftype):
    gsline = [ line for line in open(filename) if 'energy' in line][0]
    e0 = float(gsline.split(":")[1])
    tmatrixlines = []
    overlaplines = []

    # Parse the alphas, betas and the weight for mu, nu
    with open(filename) as fl:
        typestring="ftype: {}".format(ftype)

        active = False
        tmatrixactive = False
        overlapactive = False
        for line in fl:
            if typestring in line:
                active = True
            elif "ftype" in line:
                active = False
            
            if active:

                if "tmatrix" in line:
                    tmatrixactive = True
                if "overlaps" in line:
                    tmatrixactive = False
                    overlapactive = True 

                if "weight" in line:
                    weight = float(line.split(":")[1])
                if "sites" in line:
                    sitesstring = line.split(": ")[1]
                    sites = np.fromstring(sitesstring, dtype=int,sep = " ")
                if tmatrixactive:
                    tmatrixlines.append(line)
                if overlapactive:
                    overlaplines.append(line)
    tmatrix = np.loadtxt(tmatrixlines)
    overlaps = np.loadtxt(overlaplines)
    return e0, sites, tmatrix, overlaps

def lehmann_to_greensfunction(positions, weights, omegas, eta, e0, ftype):
    G = np.zeros(len(omegas), dtype=complex)
    assert(len(positions) == len(weights))
    for idx, pos in enumerate(positions):
        if "dag" in ftype:
            G += 1. / (omegas - (pos - e0) + 1j*eta) * weights[idx]
        else:
            G += 1. / (omegas + (pos - e0) + 1j*eta) * weights[idx]
    return G

def lehmann_representation(mu, nu, ftype, outfile):
    e0, sites, tmatrix, overlaps = read_e0_sites_tmatrix_overlaps(outfile, ftype)
    n_sites = len(sites)
    dim = tmatrix.shape[0]
    evals, evecs = np.linalg.eigh(tmatrix)
    psi_mu = np.zeros(dim)
    psi_nu = np.zeros(dim)
    psi_mu[:n_sites] = overlaps[:,mu]
    psi_nu[:n_sites] = overlaps[:,nu]
    weights = np.zeros(dim)
    for r, ev in enumerate(evals):
        weight = np.dot(psi_mu, evecs[:,r])*np.dot(evecs[:,r], psi_nu)
        weights[r] = weight
    return evals, weights, e0
