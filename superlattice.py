import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial import Voronoi, voronoi_plot_2d

#***********************************************************************************

def amas_reciproque(sites, sup, base=None, centre=False):
    """
    Calcule l'amas réciproque d'un amas dans l'espace réel et en fait le graphique, en 2 dimensions

    sites : liste des sites de l'amas (vecteurs 2D, composantes entières)
    sup : vecteurs du sup-réseau (2D, composantes entières)
    base : vecteurs de base du réseau (2D, composantes réelles). Optionnel. 
    centre : si vrai, alors la zone de Brillouin est centrée sur l'origine
    """


    # conversion en ndarray
    sites = np.array(sites, dtype='int')
    sup = np.array(sup, dtype='int')
    base_physique = True
    if base == None:
        base_physique = False
        base = np.array([[1,0],[0,1]])
    else:
        base = np.array(base)
    if base.shape != (2,2):
        print("La base physique n'est pas bien constituée")

    # vérification du nombre de sites dans l'amas
    n = int(np.linalg.det(sup))
    print('nombre de sites dans la sup-maille élémentaire = ', n)
    print("nombre de sites dans l'amas = ", sites.shape[0])

    # calcul des vecteurs de base du sup-réseau réciproque
    E = np.zeros((2,2), dtype='int')
    E = np.array([[sup[1,1], -sup[1,0]], [-sup[0,1], sup[0,0]]])
    print('vecteurs de base du sup-réseau réciproque (x pi) :\n', 2*E/n)

    # calcul des vecteurs d'onde du sup-réseau:
    Q = []
    for i in range(-n,n):
        for j in range(-n,n):
            q = i*E[0] + j*E[1]
            if (q[0] >= n)  or (q[1] >= n) or (q[0] < 0)  or (q[1] < 0) :
                continue
            Q.append(q)
    print('construction de ', len(Q), " vecteurs d'ondes:")
    Qr = np.array(Q, dtype='float')
    Qr *= 2/n
    print("vecteurs de l'amas réciproque (x pi):\n", Qr)

    # frontière de zone
    ZB = np.array([[0,0],[2,0],[2,2],[0,2],[0,0]])

    ibase = np.linalg.inv(base)
    if base_physique:
        if np.dot(base[0],base[1]) > 0:
            print('SVP choisir une base obtue (angle >= 90)')
            exit()
        sites = np.dot(sites,base)
        sup = np.dot(sup,base)
        Qr = np.dot(Qr,ibase.T)
        ZB = np.dot(ZB,ibase.T)

    if centre:
        F = np.array([[2,0],[0,2],[-2,2],[-2,0],[0,-2],[2,-2],[2,0]])
        F = np.dot(F,ibase.T)
        Fc = np.zeros(F.shape)
        norms = np.array([0.25*np.dot(F[i],F[i]) for i in range(F.shape[0])])
        for i in range(F.shape[0]-1):
            # print('\n', F[[i,i+1],:])
            Fc[i] = np.linalg.solve(0.5*F[[i,i+1],:], norms[i:i+2])
        Fc[F.shape[0]-1] = Fc[0]
        ZB = Fc
        print(ZB)
        for i in range(n):
            q = Qr[i]
            for p in F:
                if np.linalg.norm(q-p) < np.linalg.norm(q):
                    Qr[i,:] = q-p

    #----------------------------------------------------------
    # graphiques

    ms = 8
    # graphique dans l'espace direct
    plt.subplot(1,2,1)
    plt.gca().set_aspect(1)
    plt.grid()
    plt.xlabel('$x$')
    plt.ylabel('$y$')
    plt.plot(sites[:,0], sites[:,1], 'ko', ms=ms)
    plt.arrow(0, 0, sup[0,0], sup[0,1], width=0.01, head_width=0.1, length_includes_head=True, color='r')
    plt.arrow(0, 0, sup[1,0], sup[1,1], width=0.01, head_width=0.1, length_includes_head=True, color='r')
    plt.plot([sup[0,0]], [sup[0,1]], color='w')
    plt.plot([sup[1,0]], [sup[1,1]], color='w')


    # graphique dans l'espace réciproque
    plt.subplot(1,2,2)
    plt.gca().set_aspect(1)

    plt.plot(Qr[:,0], Qr[:,1], 'ko', ms=ms)
    plt.grid()
    plt.xlabel('$k_x/\pi$')
    plt.ylabel('$k_y/\pi$')

    # graphique de Voronoi
    # extension de la liste des points sur les zones voisines
    QrV = []
    for i,q in enumerate(Qr):
        QrV.append(q)
        QrV.append(q + ZB[1,:])
        QrV.append(q + ZB[2,:])
        QrV.append(q + ZB[3,:])
        QrV.append(q - ZB[1,:])
        QrV.append(q - ZB[2,:])
        QrV.append(q - ZB[3,:])
        QrV.append(q - ZB[1,:] + ZB[3,:])
        QrV.append(q + ZB[1,:] - ZB[3,:])
    QrV = np.array(QrV)

    vor = Voronoi(QrV)
    voronoi_plot_2d(vor, plt.gca())

    plt.xlim(-1.05,1.05)
    plt.ylim(-1.05,1.05)
    plt.plot(ZB[:,0],ZB[:,1], 'b-')
    plt.tight_layout()
    plt.show()

# ----------------------------------------------------------------------------
# EXEMPLE : AMAS de 6 sites (réseau triangulaire)
sites = [
[0,0],
[1,0],
[2,0],
[0,1],
[1,1],
[2,1],
]

# écrire ici les vecteurs du super-réseau (composantes entières seulement)
sup = [
[3,1],
[0,2]
]

# écrire ici l'expression de la base de travail en fonction de la base physique (nombres réels)
# l'exemple ici est celui d'un réseau triangulaire ou hexagonal
base = [
[1.0,0],
[-0.5, np.sqrt(3)/2]
]

# amas_reciproque(sites, sup, base=base, centre=True)
amas_reciproque(sites, sup, centre=True)

# ----------------------------------------------------------------------------
# EXEMPLE : AMAS de 8 sites (réseau carré)

sites = [
[0,0],
[1,0],
[2,0],
[0,1],
[1,1],
[2,1],
[1,-1],
[1,2]
]
sup = [
[2,-2],
[3,1]
]
amas_reciproque(sites, sup, centre=True)

# ----------------------------------------------------------------------------
# EXEMPLE : AMAS de 6 sites (réseau AF)

sites = [
[0,0],
[1,0],
[2,0],
[0,1],
[1,1],
[2,1]
]
sup = [
[3,-1],
[0,2]
]
amas_reciproque(sites, sup, centre=True)
