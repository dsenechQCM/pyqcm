import numpy as np
import pyqcm
import time

comm = None
root = False
try:
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    if comm.Get_rank() == 0:
        root = True
except ImportError:
    print('MPI not available')
    root = True

################################################################################
# exceptions


class OutOfBoundsError(Exception):
    def __init__(self, variable, iteration):
        self.variable = variable  # int
        self.iteration = iteration  # int


class TooManyIterationsError(Exception):
    def __init__(self, max_iteration):
        self.max_iteration = max_iteration  # int

class VarParamMismatchError(Exception):
    def __init__(self, numvar1, numvar2):
        self.numvar1 = numvar1  # int
        self.numvar2 = numvar2  # int

class MissingArgError(Exception):
    def __init__(self, arg):
        self.arg = arg  # int


################################################################################
# evaluation of many instances of a function in parallel

def evalF(func, x):
    n = x.shape[0]
    y = np.empty(n)
    for i in range(n):
        y[i] = func(x[i,:])
    return y    

def evalF_mpi(func, x):
    n = x.shape[0]
    nproc = comm.Get_size()
    rank = comm.Get_rank()
    y = np.zeros(n)
    y_tmp = np.zeros(n)
    k = 0  # index of points
    for i in range(n):
        if i % nproc == rank:
            y_tmp[i] = func(x[i,:])

    comm.Allreduce(y_tmp, y)
    return y


################################################################################
# quasi-Newton method

def QN_values(func, x, step):
    """Computes the function func of n variables at :math:`2n+1` points at x and at n pairs of points located at :math:`\pm` step from x in each direction. For use in the quasi-Newton method.

    :param func: function of n variables
    :param x: base point (n components NumPy array)
    :param float step: step to take in each direction in computing derivatives
    :returns: array of :math:`2n+1` values

    """
    n = len(x)
    N = 2 * n + 1
    xp = np.empty((N, n))
    for i in range(N):
        xp[i, :] = np.copy(x)
    k = 0
    for i in range(n):
        k += 1
        xp[k, i] += step[i]
        k += 1
        xp[k, i] -= step[i]

    if comm is None:
        return evalF(func, xp)
    else:    
        return evalF_mpi(func, xp)


################################################################################
# quasi-Newton method

def quasi_newton(func=None, start=None, step=None, accur=None, max=10, gtol=1e-4, bfgs=False, max_iteration=30, max_iter_diff=None):
    """Performs the quasi_newton procedure
    
    :param func: a function of N variables
    :param [float] start: the starting values
    :param [float] step: the steps used to computed the numerical second derivatives
    :param [float] accur: the required accuracy for each variable
    :param [float] max: maximum absolute value of each parameter
    :param float gtol: the gradient tolerance (gradient must be smaller than gtol for convergence)
    :param boolean bfgs: True if the BFGS method is used, otherwise the symmetric rank-1 formula is used (default)
    :param int max_iterations: maximum number of iterations, beyond which an exception is raised
    :param float max_iter_diff: maximum step to make

    """
    n = len(start)
    gradient = np.zeros(n)
    gradient0 = np.zeros(n)
    dx = np.zeros(n)
    y = np.zeros(n)
    ihessian = np.eye(n)  # inverse Hessian matrix
    iteration = 0
    x = start

    while iteration < max_iteration:
        iteration += 1

        F = QN_values(func, x, step)
        gradient0 = np.copy(gradient)
        for i in range(n):
            gradient[i] = (F[2 * i + 1] - F[2 * i + 2]) / (2 * step[i])

        if np.linalg.norm(gradient) < gtol:
            dx = -np.dot(ihessian, gradient)
            x += dx
            if root:
                print('convergence on gradient after ', iteration, ' iterations')
            break

        if iteration == 1:
            for i in range(n):
                ihessian[i, i] = step[i] * step[i] / (F[2 * i + 1] + F[2 * i + 2] - 2 * F[0])

        else:
            y = gradient - gradient0
            if bfgs:  # BFGS method
                z1 = 1.0e-14 + np.dot(y, dx)
                z1 = 1.0 / z1
                X = np.eye(n)
                X -= z1 * np.kron(y, dx).reshape((n, n))
                ihessian = np.dot(X.T, np.dot(ihessian, X))
                ihessian += z1 * np.kron(dx, dx).reshape((n, n))
            else:  # symmetric rank 1
                u = np.dot(ihessian, y)  # $u = H y_k$
                dx -= u  # the $\Delta x_k - H_k y_k$ of Wikipedia
                z1 = 1.0e-14 + np.dot(y, dx)
                z1 = 1.0 / z1
                ihessian += z1 * np.kron(dx, dx).reshape((n, n))

        dx = -np.dot(ihessian, gradient)

        if max_iter_diff is not None:
            norm_dx = np.linalg.norm(dx)
            if norm_dx > max_iter_diff:
                ratio = max_iter_diff / norm_dx
                dx *= ratio
                s1 = 'Norm of QN iteration variation dx is out of bounds: {} > {}'.format(norm_dx, max_iter_diff)
                s2 = 'Readjusting: dx = dx * {:.4}%'.format(ratio * 100)
                if root:
                    print(s1, '\n', s2)

        x += dx
        if root:
            print('QN iteration no ', iteration, '\t x = ', x)

        for i in range(n):
            if(np.abs(x[i]) > max[i]):
                raise OutOfBoundsError(variable=i, iteration=iteration)

        converged = True
        for i in range(n):
            if np.abs(dx[i]) > accur[i]:
                converged = False
                break
        if converged:
            if root:
                print('convergence on position after ', iteration, ' iterations')
            break

    if iteration == max_iteration:
        raise TooManyIterationsError(max_iteration)

    return x, gradient, ihessian



################################################################################
# Newton-Raphson method :  computation of the Hessian

def NR_Hessian(func, x, step):
    """Computes the function func of n variables at :math:`1 + 2n + n(n-1)/2` points in order to fit a quadratic form x and n pairs of points located at :math:`\pm` step from x in each direction
    
    :param func: the function
    :param [float] x: the base point 
    :param float step: step taken in each direction
    :returns (float, [float], [[float]]): the value of the function, the gradient, and the Hessian

    """
    n = len(x)
    N = 1 + 2*n + (n*(n-1))//2
    y = np.zeros(N)
    hess = np.zeros((n,n))

    xp = np.zeros((N, n))
    for k in range(N):
        xp[k, :] = np.copy(x)
 
    k = 0
    # points located at +/- step
    for i in range(n):
        k += 1
        xp[k, i] += step[i]
        k += 1
        xp[k, i] -= step[i]

    # points located off diagonal
    for i in range(n):
        for j in range(i):
            k += 1
            xp[k, i] += 0.707*(2*(i%2)-1)*step[i]
            xp[k, j] += 0.707*(2*(j%2)-1)*step[j]

    if comm is None:
        y = evalF(func, xp)
    else:    
        y = evalF_mpi(func, xp)

    # setting fit coefficients
    A = np.empty((N,N))
    A[:, 1:n+1] = xp
    for k in range(N):
        A[k,0] = 1.0
        m = n+1
        for i in range(n):
            for j in range(i+1):
                A[k, m] = xp[k, i]*xp[k,j]
                if i == j :
                    A[k, m] = xp[k, i]*xp[k,j]*0.5
                else:
                    A[k, m] = xp[k, i]*xp[k,j]        
                m += 1

    # inverting
    A1 = np.linalg.inv(A)
    y = np.dot(A1, y)
    val = y[0]
    grad = y[1:n+1]
    val += np.sum(grad*x)
    m = n+1
    for i in range(n):
        for j in range(i+1):
            hess[i,j] = y[m]
            hess[j,i] = y[m]
            if i == j :
                val += x[i] * x[j] * y[m] * 0.5
            else:
                val += x[i] * x[j] * y[m]
            m += 1
    grad += np.dot(hess,x)
    return val, grad, hess


################################################################################
# Newton-Raphson method

def newton_raphson(func=None, start=None, step=None, accur=None, max=10, gtol=1e-4, max_iteration=30, max_iter_diff=None):
    """Performs the Newton-Raphson procedure
    
    :param func: a function of N variables
    :param [float] start: the starting values
    :param [float] step: the steps used to computed the numerical second derivatives
    :param [float] accur: the required accuracy for each variable
    :param [float] max: maximum absolute value of each parameter
    :param float gtol: the gradient tolerance (gradient must be smaller than gtol for convergence)
    :param int max_iterations:  maximum number of iterations, beyond which an exception is raised
    :param float max_iter_diff: optional maximum value of the maximum step
    :returns (float, [float], [[float]]): the value of the function, the gradient, and the Hessian

    """
    n = len(start)
    gradient = np.zeros(n)
    dx = np.zeros(n)
    y = np.zeros(n)
    iteration = 0
    x = start
    step0 = step

    while iteration < max_iteration:
        iteration += 1

        gradient0 = np.copy(gradient)
        F, gradient, hessian = NR_Hessian(func, x, step)

        if np.linalg.norm(gradient) < gtol:
            dx = -np.dot(ihessian, gradient)
            x += dx
            if root:
                print('convergence on gradient after ', iteration, ' iterations')
            break

        ihessian = np.linalg.inv(hessian)
        dx = -np.dot(ihessian, gradient)

        if max_iter_diff is not None:
            norm_dx = np.linalg.norm(dx)
            if norm_dx > max_iter_diff:
                ratio = max_iter_diff / norm_dx
                dx *= ratio
                s1 = 'Norm of NR iteration variation dx is out of bounds: {} > {}'.format(norm_dx, max_iter_diff)
                s2 = 'Readjusting: dx = dx * {:.4}%'.format(ratio * 100)
                if root:
                    print(s1, '\n', s2)

        x += dx

        #redefining the steps
        step_multiplier = 2.0
        for i in range(n):
            step[i] = np.abs(dx[i])
            if step[i] > step0[i]:
                step[i] = 2.0*step0[i]
            if step[i] < step_multiplier*accur[i]:
                step[i] = step_multiplier*accur[i]

        if root:
            print('NR iteration no ', iteration, '\t x = ', x, '\t steps = ', step, '\t dx = ', dx)

        for i in range(n):
            if(np.abs(x[i]) > max[i]):
                raise OutOfBoundsError(variable=i, iteration=iteration)

        converged = True
        for i in range(n):
            if np.abs(dx[i]) > accur[i]:
                converged = False
                break
        if converged:
            if root:
                print('convergence on position after ', iteration, ' iterations')
            break

    if iteration == max_iteration:
        raise TooManyIterationsError(max_iteration)

    return x, gradient, ihessian



################################################################################

def predict_bad(x, y, p):
    for i in range(len(x)):
        if abs(x[i] - y[i]) / (abs(x[i]) + 0.01) > p:
            return True
    return False


def predict_good(x, y, p):
    for i in range(len(x)):
        if abs(x[i] - y[i]) / abs((x[i]) + 0.01) > p:
            return False
    return True

################################################################################
# performs a VCA loop with the quasi-Newton method and an external loop parameter

def vca_loop(var2sef=None, names=None, start=None, steps=None, accur=None, max=None, accur_grad=1e-6, loop_param=None, loop_start=None, loop_step=None, loop_end=None, OP_name=None, OP_small=1e-3, max_iter_diff=None, loop_param_func=None, NR=False):
    """Performs a VCA within a loop with the QN or NR method and a quadratic predictor
    
    :param var2sef: function that converts variational parameters to model parameters
    :param [str] names: names of the variational parameters
    :param [float] start: starting values
    :param [float] steps: initial steps
    :param [float] accur: accuracy of parameters (also step for 2nd derivatives)
    :param [float] max: maximum values that are tolerated
    :param float accur_grad: max value of gradient for convergence
    :param str loop_param: name of the loop parameter
    :param float loop_start: starting value of the loop parameter
    :param float loop_step: initial step of the loop parameter
    :param float loop_end: final value of the loop parameter
    :param str OP_name: name of the parameter that serves as an order parameter
    :param float OP_small: value of the order parameter that is considered small, almost zero
    :param float max_iter_diff: optional maximum value of the maximum step in the quasi-Newton method
    :param loop_param_func: optional function to call with each new value of loop_param
    :param boolean NR: True if the Newton-Raphson method is used, False if the quasi-Newton method is used
    
    """


    if root:
        print('VCA_QN procedure. loop over ', loop_param)
        if OP_name is not None:
            print('controlling the loop with the average of ', OP_name)
        else:
            print('no control of the average with an order parameter')
        if loop_param is None:
            print('The name of the control parameter is missing')
            raise MissingArgError('loop_param')
        if loop_start is None:
            print('A starting value (loop_start) of the control parameter is missing')
            raise MissingArgError('loop_start')
        if loop_step is None:
            print('A stepping value (loop_step) of the control parameter is missing')
            raise MissingArgError('loop_step')
        if loop_end is None:
            print('A end value (loop_end) of the control parameter is missing')
            raise MissingArgError('loop_end')


    if max is None:
        print('Values of maximum values (max) must be specified in VCA_QN')
        raise MissingArgError('max')

    nvar = len(start)  # number of variational parameters
    sol = np.empty((4, nvar))  # current solution + 3 previous solutions
    prm = np.empty(4)  # current loop parameter + 3 previous values

    if names is None:
        print('missing argument names : variational parameters must be specified')
        raise MissingArgError('names')

    if len(names) != len(start):
        raise VarParamMismatchError(len(start), len(names))

    if accur is None:
        accur = 1e-4*np.ones(nvar)

    if steps is None:
        steps = 10*np.array(accur)
    else:
        steps = np.array(steps)    

    def var2x(x):
        if var2sef is None:
            for i in range(len(names)): 
                pyqcm.set_parameter(names[i], x[i])
        else:
            var2sef(x)    
        pyqcm.new_model_instance()
        return pyqcm.Potthoff_functional()

    prm[0] = loop_start - loop_step
    step = loop_step
    first = True
    loop_counter = 0
    retry = False
    nretry = 0
    max_retry = 4
    while True:
        if retry:
            step = step / 2
            retry = False
            nretry += 1
            if nretry > max_retry:
                break
            if root:
                print('retrying with half the step')
        else:
            prm = np.roll(prm, 1)  # cycles the values of the loop parameter
            sol = np.roll(sol, 1, 0)  # cycles the solutions

        prm[0] = prm[1] + step  # updates the loop parameter
        if loop_end >= loop_start and prm[0] > loop_end:
            break
        if loop_end <= loop_start and prm[0] < loop_end:
            break
        if loop_param_func == None:    
            pyqcm.set_parameter(loop_param, prm[0])
            if root:
                print('---------- {:s} = {:1.4f} ----------'.format(loop_param, prm[0]))
        else:
            loop_param_func(prm[0])
    
        # predicting the starting value from previous solutions (do nothing if loop_counter=0)
        fit_type = ''
        if loop_counter == 0:
            pass
        elif loop_counter == 1:
            start = sol[1, :]  # the starting point is the previous value in the loop
        elif loop_counter > 1:
            if loop_counter == 2:
                fit_type = ' (linear fit)'
                x = prm[1:3]  # the two previous values of the loop parameter
                y = sol[1:3, :]  # the two previous values of the loop parameter
                pol = np.polyfit(x, y, 1)
            else:
                fit_type = ' (quadratic fit)'
                x = prm[1:4]  # the three previous values of the loop parameter
                y = sol[1:4, :]  # the three previous values of the loop parameter
                pol = np.polyfit(x, y, 2)
            for i in range(nvar):
                start[i] = np.polyval(pol[:, i], prm[0])

        if root:
            print('predictor : ', start, fit_type)

        try:
            if NR :
                sol[0, :], grad, iH = newton_raphson(var2x, start, steps, accur, max, accur_grad, max_iter_diff=max_iter_diff)  # Newton-Raphson process
            else:
                sol[0, :], grad, iH = quasi_newton(var2x, start, steps, accur, max, accur_grad, False, max_iter_diff=max_iter_diff)  # quasi-Newton process
        except OutOfBoundsError as E:
            print('variable ', E.variable + 1, ' is out of bounds: abs(', names[E.variable], ') > ', max[E.variable])
            if loop_counter == 0:
                print('Out of bound on starting values, aborting')
                return
            else:
                retry = True
                continue
        except TooManyIterationsError as E:
            print('quasi-Newton method failed to converge after ', E.max_iteration, ' iterations')
            if loop_counter == 0:
                print('Cannot converge on starting values, aborting')
                return 
            else:
                retry = True
                continue

        omega = var2x(sol[0, :])  # final, converged value
        if root:
            print('saddle point = ', sol[0, :])
            print('gradient = ', grad)
            print('second derivatives :', 1.0 / np.diag(iH))
            print('computing properties of converged solution...')
            print('omega = ', omega)
        ave = pyqcm.averages()
        des, val = pyqcm.properties()

        if loop_counter > 2 and retry is False:
            if(predict_good(sol[0, :], start, 0.01) and step < loop_step):
                step *= 1.5
                if root:
                    print('readjusting step to ', step)
            elif(predict_bad(sol[0, :], start, 0.1) and step > loop_step / 10):
                step /= 1.5
                if root:
                    print('readjusting step to ', step)

        loop_counter += 1

        if OP_name is not None:
            if root:
                print('ave_', OP_name, ' = ', ave[OP_name])
            if abs(ave[OP_name]) < OP_small:
                if loop_counter < 2:
                    if root:
                        print('zero order parameter on starting. aborting. Maybe try with different starting point')
                    break
                retry = True
                continue

        H = np.linalg.inv(iH)  # Hessian at the solution (inverse of iH)
        for i in range(nvar):
            val += str(H[i, i]) + '\t'

        val += time.strftime("%Y-%m-%d@%H:%M", time.localtime())
        # writes the solution in the standard file
        if root:
            fout = open('vca.tsv', 'a')
            if first:
                for i in range(nvar):
                    des += '2der_' + names[i] + '\t'
                des += 'time'    
                fout.write(des + '\n')
                first = False
            fout.write(val + '\n')
            fout.close()

    if root:
        print('*************** VCA loop ended normally ***************')


################################################################################
# performs the VCA

def vca(var2sef=None, names=None, start=None, steps=None, accur=None, max=None, accur_grad=1e-6, max_iter_diff=None, NR=False):
    """Performs a VCA with the QN or NR method
    
    :param var2sef: function that converts variational parameters to model parameters
    :param [str] names: names of the variational parameters
    :param [float] start: starting values
    :param [float] steps: initial steps
    :param [float] accur: accuracy of parameters (also step for 2nd derivatives)
    :param [float] max: maximum values that are tolerated
    :param float accur_grad: max value of gradient for convergence
    :param float max_iter_diff: optional maximum value of the maximum step in the quasi-Newton method
    :param boolean NR: True if the Newton-Raphson method is used, False if the quasi-Newton method is used
    
    """


    if names is None:
        print('missing argument names : variational parameters must be specified')
        raise MissingArgError('names')

    nvar = len(names)  # number of variational parameters

    if max is None:
        max = 10*np.ones(nvar)
    elif len(max) != nvar:
        print('the arugment "max" should have ', nvar, ' components')
        raise MissingArgError('max')

    if start is None:
        print('missing argument start')
        raise MissingArgError('start')
    elif len(start) != nvar:
        print('the arugment "start" should have ', nvar, ' components')
        raise MissingArgError('start')

    if accur is None:
        accur = 1e-4*np.ones(nvar)

    if steps is None:
        steps = 10*np.array(accur)
    elif len(steps) != nvar:
        print('the arugment "steps" should have ', nvar, ' components')
        raise MissingArgError('steps')

    def var2x(x):
        if var2sef is None:
            for i in range(len(names)): 
                pyqcm.set_parameter(names[i], x[i])
        else:
            var2sef(x)    
        pyqcm.new_model_instance()
        return pyqcm.Potthoff_functional()

    try:
        if NR :
            sol, grad, iH = newton_raphson(var2x, start, steps, accur, max, accur_grad, max_iter_diff=max_iter_diff)  # Newton-Raphson process
        else:
            sol, grad, iH = quasi_newton(var2x, start, steps, accur, max, accur_grad, False, max_iter_diff=max_iter_diff)  # quasi-Newton process
    except OutOfBoundsError as E:
        print('variable ', E.variable + 1, ' is out of bounds: abs(', names[E.variable], ') > ', max[E.variable])
        return

    except TooManyIterationsError as E:
        print('quasi-Newton method failed to converge after ', E.max_iteration, ' iterations')
        return 

    omega = var2x(sol)  # final, converged value
    if root:
        print('saddle point = ', sol)
        print('gradient = ', grad)
        print('second derivatives :', 1.0 / np.diag(iH))
        print('computing properties of converged solution...')
        print('omega = ', omega)
    ave = pyqcm.averages()
    des, val = pyqcm.properties()

    H = np.linalg.inv(iH)  # Hessian at the solution (inverse of iH)
    for i in range(nvar):
        val += str(H[i, i]) + '\t'

    val += time.strftime("%Y-%m-%d@%H:%M", time.localtime())
    # writes the solution in the standard file
    if root:
        fout = open('vca.tsv', 'a')
        for i in range(nvar):
            des += '2der_' + names[i] + '\t'
        des += 'time'    
        fout.write(des + '\n')
        fout.write(val + '\n')
        fout.close()

    if root:
        print('*************** VCA ended normally ***************')

################################################################################
def plot_sef(param, prm, accur_SEF=1e-4, show=True):
    """Draws a plot of the Potthoff functional as a function of a parameter param taken from the list prm. The results are going to be appended to 'sef.dat'
    
    :param str param: name of the parameter (independent variable)
    :param [float] prm: list of values of the parameter
    :param float accur_SEF: precision of the computation of the self-energy functional
    :param boolean show: if True, the plot is shown on the screen.
    :returns: None

    """

    pyqcm.set_global_parameter('accur_SEF', accur_SEF)
    omega = np.empty(len(prm))
    for i in range(len(prm)):
        pyqcm.set_parameter(param, prm[i])
        pyqcm.new_model_instance()
        omega[i] = pyqcm.Potthoff_functional()
        print("omega(", prm[i], ") = ", omega[i])
    
    if show:
        import matplotlib.pyplot as plt
        plt.xlim(prm[0], prm[-1])
        plt.plot(prm, omega, 'b-')
        plt.xlabel(param)
        plt.ylabel(r'$\omega$')
        plt.axhline(omega[0], c='r', ls='solid', lw=0.5)
        plt.title(pyqcm.parameter_string())
        plt.show()
