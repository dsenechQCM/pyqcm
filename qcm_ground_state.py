import numpy as np
import pyqcm


def plot_ground_state(param, prm, show=True):
    """Draws a plot of the ground state energy as a function of a parameter
    
    :param str param: name of the parameter
    :param [float] prm: list of values
    :param boolean show: if True, displays the plot on screen
    :returns: None

    """

    E = np.empty(len(prm))
    for i in range(len(prm)):
        pyqcm.set_parameter(param, prm[i])
        pyqcm.new_model_instance()
        E[i] = pyqcm.ground_state()[0]
        print("E(", prm[i], ") = ", E[i])
    
    if show:
        import matplotlib.pyplot as plt
        plt.xlim(prm[0], prm[-1])
        plt.plot(prm, E, 'bo')
        plt.xlabel(param)
        plt.ylabel(r'$E_0$')
        plt.axhline(omega[0], c='r', ls='solid', lw=0.5)
        plt.title(pyqcm.parameter_string())
        plt.show()
