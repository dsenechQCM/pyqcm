from matplotlib import pyplot as plt
import numpy as np
import numpy.ma as ma
from shapely.geometry import Polygon
from mpl_toolkits.basemap import Basemap

from matplotlib.path import Path
from matplotlib.patches import PathPatch

##setup
lonmin, lonmax = 30%360,-130%360
latmin, latmax = -30, 30
lon_sst = np.linspace(lonmin, lonmax, 50)
lat_sst = np.linspace(latmin, latmax, 50)
lons, lats = np.meshgrid(lon_sst, lat_sst);

##generating some example data
lonmid = 0.5*(lonmin+lonmax)
latmid = 0.5*(latmin+latmax)
data = (
    10*np.cos(np.deg2rad(lons-lonmid))**2*np.cos(3*np.deg2rad(lats-latmid))**2
    -10*np.exp(-0.01*((lons-(lonmin+2*lonmid)/3)**2+2*(lats-latmid)**2))
    -10*np.exp(-0.01*((lons-(lonmax+2*lonmid)/3)**2+(lats-latmid)**2))
    +10*np.exp(-0.1*((lons-(lonmid+2*lonmax)/3)**2+2*(lats-(latmid+2*latmax)/3)**2))
    )

##opening figure and axes
fig,ax = plt.subplots()

##do the Basemap stuff
m = Basemap(
    ax=ax,projection='cyl',
    llcrnrlat=-30,    urcrnrlat=30,
    llcrnrlon=30%360, urcrnrlon=-130%360,
    lat_ts=20,resolution='c'
)
x, y = m(lons, lats);
levels=[2,np.amax(data)]
cs = ax.contourf(x,y,data,levels)

##organizing paths and computing individual areas
paths = cs.collections[0].get_paths()
help(paths[0])
area_of_individual_polygons = []
for p in paths:
    sign = 1  ##<-- assures that area of first(outer) polygon will be summed
    verts = p.vertices
    codes = p.codes
    idx = np.where(codes==Path.MOVETO)[0]
    vert_segs = np.split(verts,idx)[1:]
    code_segs = np.split(codes,idx)[1:]
    for code, vert in zip(code_segs,vert_segs):

        ##visualising (not necessary for the calculation)
        new_path = Path(vert,code)
        patch = PathPatch(
            new_path,
            edgecolor = 'black' if sign == 1 else 'red',
            facecolor = 'none',
            lw =1
        )
        ax.add_patch(patch)

        ##computing the area of the polygon
        area_of_individual_polygons.append(sign*Polygon(vert[:-1]).area)
        sign = -1 ##<-- assures that the other (inner) polygons will be subtracted

##computing total area        
total_area = np.sum(area_of_individual_polygons)
formstring = ''.join(['{:+.2}' for a in area_of_individual_polygons])+'={:.2}'
print(formstring.format(*area_of_individual_polygons,total_area))

plt.show()