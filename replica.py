import numpy as np
import pyqcm
import scipy.optimize
import time
import sys
# from numba import jit

class OutOfBoundsError(Exception):
    def __init__(self, num):
        self.num = num


class TooManyIterationsError(Exception):
    def __init__(self, num):
        self.num = num


class MinimizationError(Exception):
    def __init__(self):
        pass

class VarParamMismatchError(Exception):
    def __init__(self, numvar1, numvar2):
        self.numvar1 = numvar1  # int
        self.numvar2 = numvar2  # int


# global variables --------------------------------------------------
var = []        # list of bath parameter names
n_var = 0       # number of variational parameters for one replica
n_replica = 0   # number of replicas
replica = []    # values of the bath parameters
w = []          # grid frequecies
weight = []     # distance function weights
first_time = True
Hyb = []
Hyb_down = []
n_eval = 0
dimG_tot = 0    # dimension of the lattice Green function
n_clus = 0      # number of clusters in the repeated unit
mixing = 0      # mixing code 
L = []        # array of cluster sizes
dimG = []     # array of Green function dimensions
nk = 32         # number of wavevectors on the side of the momentum grid

######################################################################
def new_model_instance(p, label=0, update=False):
    global n_var, n_replica

    for r in range(n_replica):
        for i in range(n_var):
            if np.abs(p[i+r*n_replica]) > 1000.0:
                raise OutOfBoundsError(i)
            pyqcm.set_parameter(var[i], p[i+r*n_var])
            if update == False:
                pyqcm.new_model_instance(label+1000*r)

######################################################################
def print_replica(p):
    global n_var, n_replica

    for i in range(n_var):
        line = var[i]+ ' :\t'
        for r in range(n_replica):
            line += '{0:1.6f}\t'.format(p[i+r*n_replica])
        print(line)


######################################################################
# class that contains the consolidated Green function for all clusters
# at a frequency w

class GreenFunction:
    H = []
    H_down = []

    def __init__(self, w, spin_down=False, label=0):
        self.__init2__(w, spin_down, label)

    def __init1__(self, w, spin_down=False, label=0):
        self.G = []
        self.gamma = []
        self.sigma = []
        self.w = w
        self.spin_down = spin_down
    
        for j in range(n_clus):
            d = dimG[j]
            Gc = []
            Gamma = []
            Gamma_tot = np.zeros((d, d), dtype=np.complex128)
            Gtot = np.zeros((d, d), dtype=np.complex128)
            for r in range(n_replica):
                Gc.append(pyqcm.cluster_Green_function(j, w, spin_down, label + 1000*r))
                Gamma.append(pyqcm.hybridization_function(j, w, spin_down, label + 1000*r))
                Gamma_tot += Gamma[r]
                Gtot += Gc[r]
            Gtot *= 1.0/n_replica
            self.G.append(Gtot)
            self.gamma.append(Gamma_tot)
            sig = np.linalg.inv(Gtot)
            sig -= np.eye(d) * w
            if spin_down:
                sig += self.H_down[j]
            else:
                sig += self.H[j]
            sig += Gamma_tot
            sig *= -1        
            self.sigma.append(sig)
            

    def __init2__(self, w, spin_down=False, label=0):
        self.G = []
        self.gamma = []
        self.sigma = []
        self.w = w
        self.spin_down = spin_down
    
        for j in range(n_clus):
            d = dimG[j]
            Gc = []
            Gamma = []
            Gamma_tot = np.zeros((d, d), dtype=np.complex128)
            Gtot = np.zeros((d, d), dtype=np.complex128)
            for r in range(n_replica):
                Gc.append(pyqcm.cluster_Green_function(j, w, spin_down, label + 1000*r))
                Gamma.append(pyqcm.hybridization_function(j, w, spin_down, label + 1000*r))
                Gamma_tot += Gamma[r]
            for r in range(n_replica):
                A = np.eye(d) - np.dot(Gamma_tot - Gamma[r], Gc[r])
                A = np.linalg.inv(A)
                Gc[r] = np.dot(Gc[r], A)
                Gtot += Gc[r]
            Gtot *= 1.0/n_replica
            self.G.append(Gtot)
            self.gamma.append(Gamma_tot)
            sig = np.linalg.inv(Gtot)
            sig -= np.eye(d) * w
            if spin_down:
                sig += self.H_down[j]
            else:
                sig += self.H[j]
            sig += Gamma_tot
            sig *= -1        
            self.sigma.append(sig)

    def __init3__(self, w, spin_down=False, label=0):
        self.G = []
        self.gamma = []
        self.sigma = []
        self.w = w
        self.spin_down = spin_down
    
        for j in range(n_clus):
            d = dimG[j]
            S = []
            Gamma = []
            Gamma_tot = np.zeros((d, d), dtype=np.complex128)
            S_tot = np.zeros((d, d), dtype=np.complex128)
            Gtot = np.zeros((d, d), dtype=np.complex128)
            for r in range(n_replica):
                S.append(pyqcm.cluster_self_energy(j, w, spin_down, label + 1000*r))
                Gamma.append(pyqcm.hybridization_function(j, w, spin_down, label + 1000*r))
                Gamma_tot += Gamma[r]
                S_tot += S[r]
            self.sigma.append(S_tot*(1.0/n_replica))
            self.gamma.append(Gamma_tot)
            Gtot = np.eye(d) * w - self.sigma - Gamma_tot
            if spin_down:
                Gtot -= self.H_down[j]
            else:
                Gtot -= self.H[j]
            self.G.append(np.linalg.inv(Gtot))
            

    def sigmafull(self):
        S = np.zeros((dimG_tot, dimG_tot), dtype=np.complex128)
        dc = 0
        for j in range(n_clus):
            d = dimG[j]
            S[dc:dc+d, dc:dc+d] = self.sigma[j]
            dc += d
        return S    



######################################################################
# function that computes the projected Green function
def projected_Green_function(G):
    global dimG_tot

    dim = pyqcm.spatial_dimension()
    
    G_ave = np.zeros((dimG_tot, dimG_tot), dtype=np.complex128)
    kset = np.linspace(0, 1, nk, endpoint=False)
    if dim==1:
        for kx in kset:
            k = np.array([kx,0.0,0.0])
            g = G.w*np.eye(dimG_tot)
            g -= pyqcm.tk(k, G.spin_down)
            g -= G.sigmafull()
            G_ave += np.linalg.inv(g)
        return (1.0/nk)*G_ave

    elif dim==2:
        for kx in kset:
            for ky in kset:
                k = np.array([kx, ky, 0.0])
                g = G.w*np.eye(dimG_tot)
                g -= pyqcm.tk(k, G.spin_down)
                g -= G.sigmafull()
                G_ave += np.linalg.inv(g)
            return (1.0/(nk*nk))*G_ave
    


######################################################################
# function that computes G_host
def set_grid(spin_down=False):
    """
    Computes the host Green function

    :param boolean spin_down: if True, considers the spin down sector (when mixing=4)
    :returns: an array of arrays of matrices. G_h[i], for cluster #i, is a (nw,d,d) Numpy array. with nw frequencies, and d sites.

    """
    global w, n_clus, dimG, n_eval

    n_eval = 0
    nw = len(w)
    G_h = []
    Hyb = []
    H = []
    for j in range(n_clus):
        d = dimG[j]
        G_h.append(np.zeros((nw, d, d), dtype=np.complex128))
        Hyb.append(np.zeros((nw, d, d), dtype=np.complex128))
        H.append(pyqcm.cluster_hopping_matrix(j, spin_down))
    for i in range(nw):
        G = GreenFunction(w[i], spin_down)
        Gproj = projected_Green_function(G)
        dc=0
        for j in range(n_clus):
            d = dimG[j]
            host = Gproj[dc:dc+d, dc:dc+d]
            host = np.linalg.inv(host)
            host += G.sigma[j]
            host += G.H[j]
            host -= np.eye(d) * w[i]
            G_h[j][i, :, :] = host
            dc += d
            Hyb[j][i, :, :] = G.gamma[j]

    return G_h, Hyb

######################################################################
# distance function
def CDMFT_distance(p):
    """
    Computes the distance function

    :param [float] p: array of bath parameter values
    :returns float: the distance function
    
    """
    global w, weight, var, n_var, G_host, dimG, n_clus, mixing, n_eval
    n_var = len(var)
    nw = len(w)

    n_eval += 1
    dmft_lab = 0
    new_model_instance(p, label=dmft_lab, update=True)
    pyqcm.update_bath(dmft_lab)
    dist = 0.0
    for i in range(nw):
        G = GreenFunction(w[i], spin_down=False, label=dmft_lab)
        for j in range(n_clus):
            if bath_size[j] == 0:
                continue
            gamma = G.gamma[j]
            gamma += G_host[j][i, :, :]
            dist0 = np.linalg.norm(gamma)
            dist0 *= dist0 * weight[i]
            dist += dist0
        if mixing == 4 :
            G = GreenFunction(w[i], spin_down=True, label=dmft_lab)
            for j in range(n_clus):
                if bath_size[j] == 0:
                    continue
                gamma = G.gamma[j]
                gamma += G_host[j][i, :, :]
                dist0 = np.linalg.norm(gamma)
                dist0 *= dist0 * weight[i]
                dist += dist0
    if mixing == 0:
        dist *= 2
    elif mixing == 3:
        dist /= 2
    return dist

######################################################################
# hybridization function array
def diff_hybrid(hyb1, hyb2):
    """
    Computes a difference in hybridization functions between the current iteration and the previous one

    :param [boolean] init: True the first time it is called, i.e. on the first iteration
    :returns float: the difference in hybridization arrays
    
    """
    global n_clus, w, weight, mixing
    nw = len(w)

    diff = 0.0
    for i in range(nw):
        for j in range(n_clus):
            diffH = hyb1[j][i,:,:] - hyb2[j][i,:,:]
            norm = np.linalg.norm(diffH)
            diff += weight[i]*norm*norm

    if mixing == 0:
        diff *= 2
    elif mixing == 3:
        diff /= 2
    diff /= nw  
    return np.sqrt(diff)


######################################################################
# main function

def replica_cdmft(varia, replica0, beta=50, wc=2.0, maxiter=64, accur=1e-3, accur_hybrid=1e-4, accur_dist=1e-8, displaymin=False, method='Nelder-Mead', file='cdmft.csv', skip_averages=False):
    """Performs the CDMFT procedure

    :param [str] varia: list of variational parameters
    :param [[float]] replica0: starting values of the replica parameters
    :param float beta: inverse fictitious temperature (for the frequency grid)
    :param float wc: cutoff frequency (for the frequency grid)
    :param int maxiter: maximum number of CDMFT iterations
    :param float accur: the procedure converges if parameters do not change by more than accur
    :param float accur_hybrid: the procedure converges on the hybridization function with this accuracy
    :param float accur_dist: convergence criterion when minimizing the distance function.
    :param boolean displaymin: displays the minimum distance function when minimized
    :param str method: method used by scipy.optimize to minimize the distance function
    :param str file: name of the file where the solution is written
    :param boolean skip_averages: if True, does NOT compute the lattice averages of the converged solution
    :returns: None

    """
    global w, weight, var, n_var, G_host, G_host_down, mixing, first_time, Gdim, nsites, n_clus, clusters, bath_size, n_replica, dimG, dimG_tot, n_eval

    # identifying the variational parameters
    var = varia
    n_var = len(var)
    print(n_var, ' variational parameters :', var, flush=True)
    if n_var == 0:
        print('CDMFT requires variational parameters...Aborting.')
        exit()

    # computing static variables of the class GreenFunction
    if len(replica0)%len(var):
        print('number of replica must be an integer!')
        exit()

    n_replica = len(replica0)//len(var)
    


    print('minimization method = ', method)

    # computing the general model metrics
    pyqcm.new_model_instance()
    mixing = pyqcm.mixing()
    print('mixing state = ', mixing)
    dimG_tot = pyqcm.Green_function_dimension()
    nsites, nbands, clusters, bath_size = pyqcm.model_size()
    L = np.array(clusters)
    n_clus = len(clusters)
    bath_size = np.array(bath_size)
    n_mixed = dimG_tot//nsites
    dimG = L*n_mixed
    dc = 0
    for j in range(n_clus):
        GreenFunction.H.append(pyqcm.cluster_hopping_matrix(j, spin_down=False))
        if mixing >= 4:
            GreenFunction.H_down.append(pyqcm.cluster_hopping_matrix(j, spin_down=True))

    # fills the values of the replicas with the initial parameters
    replica = replica0

    # CDMFT loop
    converged = False
    diffH=1e6
    replica_index = 0

    # initiaization of the replica models
    new_model_instance(replica, label=0)

    # convergence criterion in the bath parameters
    superiter = 0

    # first define the frequency grid for the distance function
    wr = np.arange((np.pi / beta), wc + 1e-6, 2 * np.pi / beta)
    w = np.ones(len(wr), dtype=np.complex128)
    w = w * 1j
    w *= wr
    nw = len(w)
    weight = np.ones(nw)
    weight *= 1.0 / nw
    dist_function = 'sharp_wc_{0:.1f}_beta_{1:.1f}'.format(wc, beta)

    # CDMFT loop
    converged = False
    diffH=1e6
    while True:
        new_model_instance(replica)
        params = pyqcm.parameters() # params is a dict

        # initializes G_host
        if superiter > 0:
            Hyb0 = Hyb
            if mixing == 4:
                Hyb_down0 = Hyb_down
        G_host, Hyb = set_grid()
        if mixing == 4:
            G_host_down, Hyb_down = set_grid(True)

        if method == 'Nelder-Mead':
            sol = scipy.optimize.minimize(CDMFT_distance, replica, method='Nelder-Mead', options={'disp':displaymin, 'maxfev':1000000, 'fatol': accur_dist})
        elif method == 'Powell':
            sol = scipy.optimize.minimize(CDMFT_distance, replica, method='Powell', options={'disp':displaymin, 'direc':np.identity(n_replica*n_var)*0.005, 'ftol': accur_dist, 'maxfev':1000000})
        elif method == 'BFGS':
            sol = scipy.optimize.minimize(CDMFT_distance, replica, method='BFGS', options={'disp':displaymin, 'eps':1e-4, 'maxiter':1000, 'gtol': accur_dist, 'maxfev':1000000})
        else:
            print('unknown method specified for minimization')
            exit()   

        if sol.success is False:
            print(sol)
            raise MinimizationError()

        dist_value = sol.fun
        # checking convergence on the parameters (note the sqrt(n_var) factor in order not to punish large parameter sets)
        if np.linalg.norm(replica - sol.x) < accur*np.sqrt(n_var):
            converged = True
            print('\nCDMFT converged on the parameters:', flush=True)
            break

        # checking convergence on the hybridization matrix
        if superiter > 0:
            diffH = diff_hybrid(Hyb, Hyb0)
            if mixing == 4:
                diffH += diff_hybrid(Hyb_down, Hyb_down0)
            if diffH < accur_hybrid:
                print('\nCDMFT converged on the hybridization function:', flush=True)
                converged = True
                break

        # updating the bath parameters (replace old by new)
        replica = sol.x
        new_model_instance(sol.x)

        # writing the parameters in a progress file
        # >>>  à compléter  <<<

        

        superiter += 1
        print('CDMFT iteration {:d} : {:s}, distance = {:.5f}, diff hybrid = {:.5f}, n_dist = {:d}'.format(superiter, np.array_str(sol.x), dist_value, diffH, n_eval))
        if superiter > maxiter:
            raise TooManyIterationsError(maxiter)

    # here we have converged
    if converged:
        print_replica(replica)

    else:
        print('Failure of the CDMFT algorithm', flush=True)


