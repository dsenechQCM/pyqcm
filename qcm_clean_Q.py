import numpy as np
from io import BytesIO
import re

residue_tol = 1e-9

def clean_Q(file, tol=1e-4, comp=False):
    buff = ''
    fin = open(file)
    lines = fin.readlines()
    nlines = len(lines)
    current_line = 0
    while current_line < nlines-1:
        for i in range(current_line, nlines):
            if lines[i] == 'state\n':
                buff += lines[i]+lines[i+1]
                S = lines[i+2].split()
                M = int(S[1])
                N = int(S[2])
                print('cleaning Q matrix (', M, ' x ', N, ')')
                clean_lines = clean_state(M, N, lines[i+3:i+3+N], tol, comp)
                buff += clean_lines
                current_line = i+2+N
                break
            else:
                buff += lines[i]
    fout = open('clean.out', 'w')
    fout.writelines(buff)
    fout.close()            

def clean_state(M, N, lines, tol, comp):
    W = np.empty(N)
    # reading
    if comp:
        Q = np.empty((N, M), dtype=complex)         
        for i in range(N):
            L = lines[i].split()
            W[i] = float(L[0])
            for j in range(1,M+1):
                I = L[j].find(',')
                Q[i, j-1] = float(L[j][1:I]) + float(L[j][I+1:-1])*1j
    else:
        Q = np.empty((N, M))         
        for i in range(N):
            L = lines[i].split()
            W[i] = float(L[0])
            for j in range(1,M+1):
                Q[i, j-1] = float(L[j])

    # cleaning procedure and checks     
    W2, Q2 = clean_matrix(W, Q, tol)

    A = np.trace(np.dot(np.conjugate(Q.T), Q))/M
    print('normalization check (before): ', A)
    A = np.trace(np.dot(np.conjugate(Q2.T), Q2))/M
    print('normalization check (after ): ', A)

    # writing
    buff = 'w\t{:d}\t{:d}\n'.format(Q2.shape[1], W2.shape[0])
    if comp:
        for i in range(W2.shape[0]):
            buff += '{:1.10f}'.format(W2[i])
            for j in range(M):
                buff += '\t({:1.10f}, {:1.10f})'.format(Q2[i,j].real, Q2[i,j].imag)
            buff += '\n'    
    else:        
        for i in range(W2.shape[0]):
            buff += '{:1.10f}'.format(W2[i])
            for j in range(M):
                buff += '\t{:1.10f}'.format(Q2[i,j])
            buff += '\n'    
    return buff



def clean_matrix(W, Q, tol):
    global residue_tol
    I1 = 0
    I2 = 1
    N = 0
    while I1 < W.shape[0]:
        for i in range(I1+1, W.shape[0]):
            if np.abs(W[i]-W[I1]) > tol:
                I2 = i
                break;
        if I2 == I1:
            I2 = W.shape[0]
        print(I1, I2,'\n')
        if I2 == I1+1:
            W[N] = W[I1]
            Q[N, :] = Q[I1, :]
            N += 1
        else:
            ave = np.average(W[I1:I2])
            A = np.dot(Q[I1:I2, :].T, np.conjugate(Q[I1:I2, :]))
            e, U = np.linalg.eigh(A)
            kept = 0
            print('eigs = ', e, '\n')
            for i in range(len(e)):
                if np.abs(e[i] > residue_tol):
                    norm = np.sqrt(e[i])
                    W[N] = ave
                    Q[N, :] = norm*U[:, i]
                    N += 1
                    print('w = ', ave)
        I1 = I2

    return W[0:N], Q[0:N, :]




