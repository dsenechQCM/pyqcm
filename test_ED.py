import numpy as np

def hopping_matrix_element(s, r1, r2):
    mask1 = 1 << r1
    mask2 = 1 << r2
    if (mask1&s) == 0 or (mask2&s) :
        return s, 0
    sp = (s^mask1)^mask2
    if r1>r2:
        inter_mask = mask1 - (mask2<<1)
    else:
        inter_mask = mask2 - (mask1<<1)
    sign = 1 - 2*(bin(s&inter_mask).count('1')%2)
    return sp, sign

# defining a basis with nup spin-up electrons and ndown spin-down electrons
def basis(nup, ndown, L):
    down = spin_basis(ndown, L)
    up = spin_basis(nup, L)
    return [ d + (u<<L) for d in down for u in up]



def spin_basis(n, L):
    if n<0 or n>L:
        print('n has to be between 0 and L')
        exit()
    B = []
    for b in range(1<<L):
        if(bin(b).count('1') == n):
            B.append(b)
    return B



def hopping_operator(basis, elements):
    dim = len(basis)
    op = np.zeros((dim, dim))
    for i in range(dim):
        s = basis[i]
        for x in elements:
            sp, phase = hopping_matrix_element(s, x[0], x[1])
            if phase != 0:
                j = basis.index(sp)
                op[i, j] = phase
                op[j, i] = np.conj(phase)
    return op            


def U_operator(basis, L):
    dim = len(basis)
    op = np.zeros((dim, dim))
    for i in range(dim):
        s = basis[i]
        op[i,i] = bin((s<<L)&s).count('1')
    return op

def chemical_potential(basis, L):
    dim = len(basis)
    op = np.zeros((dim, dim))
    for i in range(dim):
        s = basis[i]
        op[i,i] = -bin(s).count('1')
    return op


L = 2
nup = 1
ndown = 1

t_elements = [(i, i+1) for i in range(L-1)]
t_elements += [(i+L, i+1+L) for i in range(L-1)]

mu_elements = [(i,i) for i in range(2*L)]

t_op = hopping_operator(basis(nup, ndown, L), t_elements)
mu_op = chemical_potential(basis(nup, ndown, L), mu_elements)
U_op = U_operator(basis(nup, ndown, L), L)

U = 4.0
t = -1
mu = U/2
H = t*t_op + U*U_op + mu*mu_op

print('H = \n', H)

print(np.linalg.eig(H))
