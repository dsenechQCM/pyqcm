from pyqcm import *
from pyqcm.spectral import *

new_cluster_model('L2', 2, 0)
ns = 2
new_cluster_operator('L2', 'Dc', 'anomalous', [
    (1, 2+ns, 1.0),
    (2, 1+ns, 1.0)
])

add_cluster('L2', [0, 0, 0], [[0, 0, 0], [1, 0, 0]])
lattice_model('1D-2', [[2, 0, 0]])
interaction_operator('U')
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
anomalous_operator('D', [1, 0, 0], 1)  # NN singlet

set_global_parameter('print_Hamiltonian')
set_target_sectors(['R0:S0'])
set_parameters("""
U = 1e-8
t = 1
D_1 = 1.0
""")

from pyqcm.vca import *
plot_sef('D_1', np.arange(0.1, 1.0, 0.1), 1e-6, show=True)

#===============================================================================
# expected results

# the SEF (omega) should be a constant = -1.2732395