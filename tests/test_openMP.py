from pyqcm import *

# defining the model (12-site chain)
L = 12
sym = [ L-i for i in range(L)]; new_cluster_model('clus', L, 0, [sym])
# new_cluster_model('clus', L, 0)
pos = []
for i in range(L):
    pos += [[i,0,0]]


add_cluster('clus', [0, 0, 0], pos)
lattice_model('1D_{:d}'.format(L), [[L, 0, 0]])

interaction_operator('U')
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
anomalous_operator('D', [1, 0, 0], -1) 

# computing averages
# set_global_parameter('CSR_sym_store')
set_global_parameter('print_Hamiltonian')
set_global_parameter('verbose', 0)
set_global_parameter('accur_OP', 1e-6)
# set_target_sectors(['R0:N'+str(L)+':S0'])
set_target_sectors(['R0:N12:S0'])
set_parameters("""
U = 4
t = 1
mu = 2
""")

I = new_model_instance(record=True)
I.print('test_openMP_record.py')
print_cluster_averages(cluster_averages())
from pyqcm.spectral import *
spectral_function(path='line', file='spectre.pdf')
# print_averages(averages())


#===============================================================================
# expected results

# cluster averages
#    <U> = 0.094783
#    <mu> = 1.000000
#    <t> = -0.922986

# lattice averages
#    <mu> = 1.000000
#    <t> = -0.959215
