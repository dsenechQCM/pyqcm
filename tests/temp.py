from pyqcm import *
from pyqcm.spectral import *
set_global_parameter('nosym')
import model_1D_4

set_target_sectors(['R0'])
set_parameters("""
t=1
S = 0.4
# H = 0.5
Hx = 1
mu = 1
""")

nk = 32
new_model_instance()
spectral_function(wmax = 6, nk = nk, path='line')


set_target_sectors(['R0:N4:S0'])
set_parameter('Hx', 0.0)
set_parameter('S', 0.0)
new_model_instance()
spectral_function(wmax = 6, nk = nk, path='line')

set_target_sectors(['R0:N4'])
set_parameter('Hx', 0.2)
new_model_instance()
spectral_function(wmax = 6, nk = nk, path='line')

set_target_sectors(['R0:S0'])
set_parameter('Hx', 0.0)
set_parameter('S', 0.4)
new_model_instance()
spectral_function(wmax = 6, nk = nk, path='line')

set_target_sectors(['R0:N4:S0'])
set_parameter('S', 0.0)
set_parameter('H', 0.0)
new_model_instance()
spectral_function(wmax = 6, nk = nk, path='line')

