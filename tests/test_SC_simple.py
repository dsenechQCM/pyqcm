from pyqcm import *
from pyqcm.spectral import *

new_cluster_model('L2', 2, 0)
ns = 2
new_cluster_operator('L2', 'Dc', 'anomalous', [
    (1, 2+ns, 1.0),
    (2, 1+ns, 1.0)
])

add_cluster('L2', [0, 0, 0], [[0, 0, 0], [1, 0, 0]])
lattice_model('1D-2', [[2, 0, 0]])
interaction_operator('U')
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
anomalous_operator('D', [1, 0, 0], 1)  # NN singlet

set_global_parameter('print_Hamiltonian')
set_target_sectors(['R0:S0'])
set_parameters("""
U = 1e-8
D = 1.0
""")

new_model_instance()
print(ground_state())
spectral_function(path='line')
print_model('model.out')

#===============================================================================
# expected results

# ground state : [(-1.999999995, 'R0:S0:1')]

# Hamiltonian (dense form):
# 0       0       -1      -1      0       0
# 0       0       0       0       0       0
# -1      0       0       0       0       -1
# -1      0       0       0       0       -1
# 0       0       0       0       0       0
# 0       0       -1      -1      0       0

# hopping matrix from model.out:
# 0	0	0	-1	
# 0	0	-1	0	
# 0	-1	0	0	
# -1	0	0	0	

# spectral function: double cosine with amplitude 2.