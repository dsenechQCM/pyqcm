from pyqcm import *
from pyqcm.spectral import *

def oneD_model_C2(L):
    sym = [ L-i for i in range(L)]
    new_cluster_model('clus', L, 0, [sym])
    pos = []
    for i in range(L):
        pos += [[i,0,0]]

    add_cluster('clus', [0, 0, 0], pos)
    lattice_model('1D_{:d}'.format(L), [[L, 0, 0]])
    
    interaction_operator('U')
    interaction_operator('J',  link=[1, 0, 0], type='Hund')
    interaction_operator('Up', link=[1, 0, 0])
    hopping_operator('t', [1, 0, 0], -1)  # NN hopping
    hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
    hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)  # on-site spin flip
    anomalous_operator('D', [1, 0, 0], 1)  # NN singlet
    anomalous_operator('S', [0, 0, 0], 1)  # on-site singlet
    anomalous_operator('Si', [0, 0, 0], 1j)

    density_wave('h', 'Z', [0, 0, 0])
    density_wave('hx', 'X', [0, 0, 0])

def oneD_model(L):
    new_cluster_model('clus', L, 0)
    pos = []
    for i in range(L):
        pos += [[i,0,0]]

    add_cluster('clus', [0, 0, 0], pos)
    lattice_model('1D_{:d}'.format(L), [[L, 0, 0]])
    
    interaction_operator('U')
    interaction_operator('J',  link=[1, 0, 0], type='Hund')
    interaction_operator('Up', link=[1, 0, 0])
    hopping_operator('t', [1, 0, 0], -1)  # NN hopping
    hopping_operator('ti', [1, 0, 0], -1, tau=2)  # NN hopping (imaginary)
    hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
    hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)  # on-site spin flip
    anomalous_operator('D', [1, 0, 0], 1)  # NN singlet
    anomalous_operator('S', [0, 0, 0], 1)  # on-site singlet
    anomalous_operator('Si', [0, 0, 0], 1j)

    density_wave('h', 'Z', [0, 0, 0])
    density_wave('hx', 'X', [0, 0, 0])


def oneD_model_per(L):
    sym = [i+2 for i in range(L)]
    sym[-1] = 1
    new_cluster_model('clus', L, 0, [sym])
    pos = []
    for i in range(L):
        pos += [[i,0,0]]

    add_cluster('clus', [0, 0, 0], pos)
    lattice_model('1D_{:d}'.format(L), [[L, 0, 0]])
    
    interaction_operator('U')
    interaction_operator('J',  link=[1, 0, 0], type='Hund')
    interaction_operator('Up', link=[1, 0, 0])
    hopping_operator('t', [1, 0, 0], -1)  # NN hopping
    hopping_operator('ti', [1, 0, 0], -1, tau=2)  # NN hopping (imaginary)
    hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
    hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)  # on-site spin flip
    anomalous_operator('D', [1, 0, 0], 1)  # NN singlet
    anomalous_operator('S', [0, 0, 0], 1)  # on-site singlet
    anomalous_operator('Si', [0, 0, 0], 1j)

    density_wave('h', 'Z', [0, 0, 0])
    density_wave('hx', 'X', [0, 0, 0])
