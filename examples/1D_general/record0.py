from pyqcm import *
set_global_parameter("nosym")
new_cluster_model('clus', 6, 0, generators=None, bath_irrep=False)
add_cluster('clus', [0, 0, 0], [[0, 0, 0], [1, 0, 0], [2, 0, 0], [3, 0, 0], [4, 0, 0], [5, 0, 0]])
lattice_model('1D_6', [[6, 0, 0]], None)
interaction_operator('U')
interaction_operator('J', link=[1, 0, 0], type='Hund')
interaction_operator('Up', link=[1, 0, 0])
hopping_operator('t', [1, 0, 0], -1)
hopping_operator('ti', [1, 0, 0], -1, tau=2)
hopping_operator('tp', [2, 0, 0], -1)
hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)
anomalous_operator('D', [1, 0, 0], 1)
anomalous_operator('S', [0, 0, 0], 1)
anomalous_operator('Si', [0, 0, 0], 1j)
density_wave('h', 'Z', [0, 0, 0])
density_wave('hx', 'X', [0, 0, 0])

try:
    import model_extra
except:
    pass		
set_target_sectors(['R0:N6:S0'])
set_parameters("""

ti =1
U = 1e-8
mu= 0.5*U
""")
set_parameter("U", 1e-08)
set_parameter("ti", 1.0)

new_model_instance(0)

solution=[None]*1

#--------------------- cluster no 1 -----------------
solution[0] = """
U	1e-08
mu	5e-09
ti	1

GS_energy: -6.98792 GS_sector: R0:N6:S0:1
GF_format: bl
mixing	0
state
R0:N6:S0	-6.98792	1
w	6	6
-0.44504186791263	(-0.52112088916961,0)	(0,0.23192061392433)	(-0.41790650594128,0)	(0,0.41790650594127)	(-0.23192061392433,0)	(0,0.5211208891696)
-1.2469796037175	(-0.41790650594128,0)	(0,0.52112088916961)	(0.23192061392433,0)	(0,0.23192061392433)	(0.5211208891696,0)	(0,-0.41790650594127)
-1.8019377358048	(0.23192061392433,0)	(0,-0.41790650594127)	(-0.5211208891696,0)	(0,0.5211208891696)	(0.41790650594128,0)	(0,-0.23192061392433)
0.44504186791262	(-0.5211208891696,0)	(0,-0.23192061392433)	(-0.41790650594127,0)	(0,-0.41790650594128)	(-0.23192061392433,0)	(0,-0.52112088916961)
1.2469796037175	(-0.41790650594127,0)	(0,-0.5211208891696)	(0.23192061392433,0)	(0,-0.23192061392433)	(0.5211208891696,0)	(0,0.41790650594128)
1.8019377358048	(-0.23192061392433,0)	(0,-0.41790650594128)	(0.5211208891696,0)	(0,0.5211208891696)	(-0.41790650594127,0)	(0,-0.23192061392433)

"""
read_cluster_model_instance(solution[0], 0)

from pyqcm.spectral import *
spectral_function(wmax = 6, nk = 32, path='line')
