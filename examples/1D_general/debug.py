from pyqcm import *
from pyqcm.spectral import *
# set_global_parameter('nosym')
# set_global_parameter('periodic')
from oneD_C2 import *

#--------------------------------------------------------------------------
# set_target_sectors(['R0:N3:S1/R0:N3:S-1'])

# set_global_parameter('modified_Lanczos')
# set_global_parameter('max_iter_lanczos', 1200)
# set_global_parameter('periodic')
set_global_parameter('verbose', 1)
# set_global_parameter('GF_solver', 'cf')
# set_global_parameter('Hamiltonian_format', 'factorized')
set_global_parameter('Hamiltonian_format', 'onthefly')
# set_global_parameter('accur_lanczos', 1e-14)
oneD_model(8)
set_target_sectors(['R0:S1'])
set_parameters("""
t =1
U = 4
mu= 0.5*U
S = 0.5
""")
# print_model("model.out")

import timeit
# I = new_model_instance(record=True)
# I.print('record.py')
t_calcul = timeit.default_timer()
new_model_instance()
# print_cluster_averages(cluster_averages())

# print(print(ground_state()))
# print(print_wavefunction())

t_calcul = timeit.default_timer() - t_calcul
print('temps de calcul = ', t_calcul)

spectral_function(wmax = 6, nk = 32, path='line', file='tmp1.pdf')
# cluster_spectral_function(wmax=4,offset=6,full=False, file='tmp1.pdf')
# print(print(ground_state()))
