from pyqcm import *
import model_1D_4_4b

set_global_parameter('max_dim_full', 258)
set_global_parameter('verbose', 6)
set_global_parameter('seed', 999)
set_global_parameter('temperature', 0.001)
set_target_sectors(['R0:S0'])
set_global_parameter("nosym")

set_parameters("""
t = 1, 
U=4
mu = 0.5 * U
tb1_1 = 0.5
tb2_1 = -1*tb1_1
eb1_1 = 1
eb2_1 = -1*eb1_1
D = 0.1
""")

new_model_instance()
I = new_model_instance(record=True)
I.print()
print_cluster_averages(cluster_averages())
