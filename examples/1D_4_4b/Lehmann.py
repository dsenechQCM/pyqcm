import numpy as np
from pyqcm import *
from pyqcm.spectral import *
import model_1D_4_4b

set_target_sectors(['R0:N8:S0'])
set_parameters([
    ('U', 4),
    ('mu', 0.5, 'U'),
    ('t', 1),
    # ('D', 0.00001),
    ('tb1_1', 0.611),
    ('tb2_1', 1, 'tb1_1'),
    ('eb1_1', 1.162),
    ('eb2_1', -1, 'eb1_1')
])

print_model('model.out')
new_model_instance()
spectral_function_Lehmann(path='halfline', offset=0.02, nk=64, band=1)





