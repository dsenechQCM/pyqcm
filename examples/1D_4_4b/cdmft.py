import numpy as np
from pyqcm import *
from pyqcm.cdmft import cdmft
import model_1D_4_4b

# set_global_parameter('kgrid_side', 32)
set_global_parameter('potential_energy')
sec = ['R0:N8:S0']
set_target_sectors(sec)
set_parameters("""
    U=4
    mu=0.5*U
    t=1
    tp = -0.3
    tb1_1 = 0.5
    tb2_1 = 0.6
    eb1_1 = 0.2
    eb2_1 = 0.1
""")

cdmft(['tb1_1', 'eb1_1', 'tb2_1', 'eb2_1'], target_sectors = sec, accur=1e-4)

print('cluster averages:')
print_cluster_averages(cluster_averages())

