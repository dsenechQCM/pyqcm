from pyqcm import *
from pyqcm.spectral import *
import qcm_ED
from pyqcm.wietek import wietek_solver
import timeit

# set_global_parameter('periodic')
set_global_parameter('nosym')
import model_1D_4_4b

# set_global_parameter('max_dim_full', 5000)
set_global_parameter('verbose', 5)
cf = False
# set_global_parameter('GF_solver', 'cf'); cf = True
set_global_parameter('accur_band_lanczos', 1e-15)

sec = 'R0:N8:S0'
set_target_sectors([sec])

set_parameters("""
t = 1, 
U=4
mu = 0.5 * U
tb1_1 = 0.5
tb2_1 = -1*tb1_1
eb1_1 = 1
eb2_1 = -1*eb1_1
""")

t = timeit.default_timer()
set_global_parameter('max_iter_BL', 215)
set_global_parameter('accur_band_lanczos', 0.0)
I = new_model_instance(record=True, label=1)
I.print('record.py')
t = timeit.default_timer() - t
print('temps de calcul (qcm) : ', t)
w, A1 = cluster_spectral_function(8, full=True, label=1)
print_cluster_averages(cluster_averages())
# spectral_function(path='line', label=1)

t = timeit.default_timer()
new_model_instance()
wietek_solver(sec, cf=cf)
t = timeit.default_timer() - t
print('temps de calcul (qcm) : ', t)
w, A2 = cluster_spectral_function(8, full=True)
print_cluster_averages(cluster_averages())
# spectral_function(path='line')

plot_spectral_array(w, A1-A2, offset=0.1)