from pyqcm import *
from pyqcm.cdmft import cdmft
from pyqcm.loop import *
import model_1D_4_4b
import os

np.set_printoptions(precision=4, linewidth=512, suppress=True)

set_global_parameter('verbose',1)
set_global_parameter('accur_OP',1e-3)

set_target_sectors(['R0:N8:S0'])
set_parameters("""
    U=4
    mu=2
    t=1
    tb1_1=0.5
    tb2_1=0.5
    eb1_1=1
    eb2_1=-1
""")

varia=['tb1_1', 'tb2_1', 'eb1_1', 'eb2_1']

# for mu in np.arange(3, 2, -0.05):
#     set_parameter('mu', mu)
#     new_model_instance()
#     cdmft(varia=varia, accur=1e-4, accur_hybrid=1e-8, skip_averages=True)
#     print('\n ************ mu = ', mu)


target = 0.95
def func(mu):
    set_parameter('mu', mu)
    new_model_instance()
    print('-------> mu = ', mu)
    cdmft(varia=varia, accur=1e-5, accur_hybrid=1e-8, maxiter=128, skip_averages=True)
    ave = cluster_averages()
    print(ave)
    n = ave['mu'][0]
    # n = averages()['mu']
    print('----------- density = ', n)
    return n - target

fixed_density_loop(
    mu=-0.1,
    func=func,
    loop_param='U', 
    loop_values=np.arange(0.2, 8, 0.2),
    var_param = varia,
    dens_tol = 0.001,
    dir = os.path.dirname(os.path.abspath(__file__))
) 