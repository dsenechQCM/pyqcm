from pyqcm import *

nb = 4
ns = 4
no = ns+nb
new_cluster_model('clus', ns, nb)

new_cluster_operator('clus', 'tb1', 'one-body', [
    (1, 5, -1.0),
    (4, 6, -1.0),
    (1+no, 5+no, -1.0),
    (4+no, 6+no, -1.0),
])

new_cluster_operator('clus', 'tb2', 'one-body', [
    (1, 7, -1.0),
    (4, 8, -1.0),
    (1+no, 7+no, -1.0),
    (4+no, 8+no, -1.0)
])

new_cluster_operator('clus', 'eb1', 'one-body', [
    (5, 5, 1.0),
    (6, 6, 1.0),
    (5+no, 5+no, 1.0),
    (6+no, 6+no, 1.0)
])

new_cluster_operator('clus', 'eb2', 'one-body', [
    (7, 7, 1.0),
    (8, 8, 1.0),
    (7+no, 7+no, 1.0),
    (8+no, 8+no, 1.0)
])

new_cluster_operator('clus', 'sb1', 'anomalous', [
    (5, 6+no, 1.0),
    (6, 5+no, -1.0)
])

# construction of the lattice model

add_cluster('clus', [0, 0, 0], [[0, 0, 0], [1, 0, 0], [2, 0, 0], [3, 0, 0]])
lattice_model('1D_4_4b', [[4, 0, 0]])
interaction_operator('U')
interaction_operator('J',link=[1,0,0], type='Hund')
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
hopping_operator('hx', [0, 0, 0], 1, tau=0, sigma=1) 
anomalous_operator('D', [1, 0, 0], -1.0)
