from pyqcm import *
from pyqcm.spectral import *
from pyqcm.vca import *
import model_2x2_C2

accur = 0.0005
qcm.set_global_parameter('accur_OP', 1e-4)
qcm.set_global_parameter('verbose', 0)
qcm.set_global_parameter('accur_SEF', 1e-4)
set_target_sectors(['R0:N4:S0'])
set_parameters("""
t = 1
U = 4
mu = 0.5*U
M = 0
M_1 = 0.15
""")

loop_param = 'U'
var_param = 'M_1'
loop = np.arange(2, 10, 0.5)


def var2sef(x):
    qcm.set_parameter(var_param, x[0])


vca_loop(var2sef=var2sef, names=['M_1'], start=[0.1], accur=[5e-5], max=[1], loop_param='U', loop_start=6, loop_end=1.0, loop_step=-0.2, OP_name='M')
