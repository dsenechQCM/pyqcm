from pyqcm import *
from pyqcm.spectral import *
import model_2x2_C2

set_target_sectors(['R0:N4:S0'])
set_parameters("""
U = 1e-6
t = 1
""")
new_model_instance()


def eta(x):
    return 0.05 + 0.05 * np.abs(x)

w = [x + eta(x) * 1j for x in np.linspace(-6, 6, 200)]
DoS(w)
