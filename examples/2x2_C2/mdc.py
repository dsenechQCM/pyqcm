from pyqcm import *
from pyqcm.spectral import *
import model_2x2_C2
import numpy as np

set_target_sectors(['R0:N4:S0'])
set_parameters("""
U = 2
mu = 1
t = 1
t2 = -0.3
t3 = 0.2
""")
new_model_instance()
mdc()
# mdc_anomalous(w=.0+0.1j, bands=np.array([[1.0]]), self=True)
