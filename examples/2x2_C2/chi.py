from pyqcm import *
from pyqcm.spectral import *
import matplotlib.pyplot as plt
import model_2x2_C2

set_target_sectors(['R0:N4:S0/R1:N4:S0'])
set_parameters("""
t = 1
U = 4
mu = 0.5*U
M = 0.1
""")
new_model_instance()
w = np.arange(0,6,0.02)+0.05j
chi = susceptibility('M@1', w)
print(susceptibility_poles('M@1'))
plt.plot(w.real, -chi.imag)
plt.show()
