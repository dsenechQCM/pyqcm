from pyqcm import *
import model_2x2_C2


set_target_sectors(['R0:S0'])
set_parameters("""
U = 1e-6
t = 1
D = 0.1
""")
for j in range(1,5):
    set_parameter('D', j*0.1)
    new_model_instance()
    print('ground state : ', ground_state())

