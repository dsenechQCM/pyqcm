from pyqcm import *
from pyqcm.profile import *
set_global_parameter('nosym')
import model_2x2_C2

set_global_parameter('max_dim_full', 300)
set_target_sectors(['R0'])
set_parameters("""
mu = 0.5*U
U = 0.00
t = 1
hx = 0.1
D = 0.1
""")
new_model_instance()
plot_profile(bond_spin_scale=1, spin_angle=0, n_scale=0.5, singlet_scale=1, triplet_scale=1)
