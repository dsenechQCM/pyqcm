from pyqcm import *
from pyqcm.spectral import *
import model_2x2_C2
import numpy as np

set_target_sectors(['R0:N4:S0'])
set_parameters("""
U = 2
mu = 1.8
t = 1
t2 = -0.3
t3 = 0.2
""")
new_model_instance()
mdc(opt='Z')


# import matplotlib.pyplot as plt
# U = np.arange(0.1, 8, 0.1)
# Z = np.zeros(len(U))
# for i in range(len(U)):
#     set_parameter('U', U[i])
#     new_model_instance()
#     Z[i] = cluster_QP_weight(band=1)

# plt.plot(U,Z,'bo-')
# plt.show()