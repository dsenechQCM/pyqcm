from pyqcm import *
from pyqcm.vca import *
import model_2x2_C2

set_global_parameter("max_dim_full", 300)
set_target_sectors(['R0:S0'])
set_parameters("""
t = 1
U = 8
mu = 1.8
D = 0
D_1 = 0.01
""")

plot_sef('D_1', np.arange(0.01, 0.3, 0.01))
