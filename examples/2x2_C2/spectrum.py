from pyqcm import *
from pyqcm.spectral import *
set_global_parameter('nosym')
import model_2x2_C2

set_target_sectors(['R0:N4:S0'])
set_parameters("""
t = 1
U = 8
mu = 0.5*U
""")
new_model_instance()
spectral_function(6.0, path='triangle', nk=32)
# cluster_spectral_function(5, full=True, offset=6,file='old.pdf')
