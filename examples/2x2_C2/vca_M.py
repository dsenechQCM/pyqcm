from pyqcm import *
from pyqcm.spectral import *
from pyqcm.vca_dev import *
import model_2x2_C2

accur = 0.0005
set_global_parameter('accur_OP', 1e-4)
set_global_parameter('verbose', 7)
set_global_parameter('accur_SEF', 1e-4)
set_target_sectors(['R0:S0'])
set_parameters("""
t = 1
U = 4
mu = 1.8
D = 0
D_1 = 0.25
""")

# vca_loop(names=['M_1'], start=[0.1], accur=[5e-5], max=[1],loop_param='U', loop_start=8, loop_step=-0.2, loop_end=2, OP_name='M')
vca_loop(names=['D_1'], start=[0.1], accur=[5e-5], max=[1],loop_param='mu', loop_start=1.8, loop_step=-0.02, loop_end=1, OP_name='D')
