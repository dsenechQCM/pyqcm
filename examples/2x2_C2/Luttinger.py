from pyqcm import *
from pyqcm.spectral import *
import model_2x2_C2
import numpy as np

set_target_sectors(['R0:S0'])
set_parameters("""
U = 8
mu = 4
D = 0.0
D_1 = 0.2
t = 1
t2 = -0.3
t3 = 0.2
""")
new_model_instance()
Luttinger_surface(nk=200)
