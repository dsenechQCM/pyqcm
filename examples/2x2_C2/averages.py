from pyqcm import *
from pyqcm.spectral import *
import model_2x2_C2

set_target_sectors(['R0:N4:S0'])
set_parameters("""
t = 1
U = 2
mu = 0.5*U
""")
new_model_instance()
print_averages(averages())
