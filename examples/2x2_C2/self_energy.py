from pyqcm import *
from pyqcm.spectral import *
import model_2x2_C2

set_target_sectors(['R0:N4:S0'])
set_parameters("""
t = 1
U = 4
mu = 0.5*U
""")
new_model_instance()
spectral_function(wmax=10, path='triangle', nk=32, offset=25, self=True)
