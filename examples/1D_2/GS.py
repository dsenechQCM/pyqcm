import numpy as np
from pyqcm import *
import model_1D_2

set_target_sectors(['R0:N2:S0'])
set_parameters("""
t = 1
mu = 2
U = 4
""")

# loop over chemical potential and computation of the ground state energy and averages at each value
for mu in np.arange(-0.1,0.1,0.01):
    set_parameter('mu', mu)
    new_model_instance()
    print('\nmu = {0:3.2f}'.format(mu))
    G = ground_state()
    print('G.S. energy: {:6.6f}\tsector: {:s}'.format(G[0][0], G[0][1]))
    print_averages(cluster_averages())

