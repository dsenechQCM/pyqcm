from pyqcm import *

new_cluster_model('L2', 2, 0, [[2,1]])
add_cluster('L2', [0, 0, 0], [[0, 0, 0], [1, 0, 0]])
lattice_model('1D-2', [[2, 0, 0]])
interaction_operator('U')
interaction_operator('J',  link=[1, 0, 0], type='Hund')
interaction_operator('Up', link=[1, 0, 0])
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)  # on-site spin flip
anomalous_operator('D', [1, 0, 0], 1)  # NN singlet
anomalous_operator('S', [0, 0, 0], 1)  # on-site singlet

density_wave('h', 'Z', [0, 0, 0])
density_wave('hx', 'X', [0, 0, 0])
