from pyqcm import *

new_cluster_model('L2', 2, 0)

add_cluster('L2', [0, 0, 0], [[0, 0, 0], [1, 0, 0]])
lattice_model('1D-2', [[2, 0, 0]])
interaction_operator('U')
interaction_operator('J',  link=[1, 0, 0], type='Hund')
interaction_operator('K',  link=[1, 0, 0], type='X')
interaction_operator('Up', link=[1, 0, 0])
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
hopping_operator('ti', [1, 0, 0], -1, tau=2)
hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)  # on-site spin flip
anomalous_operator('D', [1, 0, 0], 1)  # NN singlet
anomalous_operator('Di', [1, 0, 0], 1j)
anomalous_operator('CDi', [1, 0, 0], 1+1j)
anomalous_operator('S', [0, 0, 0], 1)  # on-site singlet
anomalous_operator('Si', [0, 0, 0], 1j)
anomalous_operator('dz', [1, 0, 0], 1, type='dz')  # NN triplet
anomalous_operator('dy', [1, 0, 0], 1, type='dy')  # NN triplet
anomalous_operator('dx', [1, 0, 0], 1, type='dx')  # NN triplet

# density_wave('M', 'Z', [1, 0, 0])
# density_wave('Mx', 'X', [1, 0, 0])
density_wave('h', 'Z', [0, 0, 0])
density_wave('hx', 'X', [0, 0, 0])
# density_wave('pT', 'dz', [1, 0, 0], amplitude=1, link=[1, 0, 0])
