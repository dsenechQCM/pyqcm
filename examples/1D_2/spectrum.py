from pyqcm import *
from pyqcm.spectral import *
set_global_parameter("nosym")
import model_1D_2

set_global_parameter("print_Hamiltonian")
set_global_parameter("max_dim_full", 64)
set_target_sectors(['R0:N2:S0'])
set_parameters("""
t = 1
U = 0.00001
""")
qcm.new_model_instance()
spectral_function(wmax=5, path='line', nk=32)
