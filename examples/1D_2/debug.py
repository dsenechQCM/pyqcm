from pyqcm import *
from pyqcm.spectral import *
import model_1D_2

set_global_parameter("verbose", 2)
set_global_parameter("print_Hamiltonian")
set_target_sectors(['R0:N2'])
set_parameters("""
U = 0
mu = 0
K = 1.0
t=0
sf = 1e-8
""")
qcm.new_model_instance()
print(ground_state())


# w = [x + 0.05j for x in np.arange(-6,6,0.01)]
# DoS(w, sum=True, progress = True, file=None)
print_model('model.out')