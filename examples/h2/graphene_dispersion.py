import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d


# d1 = np.array([0.5, np.sqrt(3)/2])
# d2 = np.array([-1.0, 0])
# d3 = np.array([0.5, -np.sqrt(3)/2])

d1 = np.array([0.5, np.sqrt(3)/2])
d2 = np.array([-1.0, 0])
d3 = np.array([0.5, -np.sqrt(3)/2])

def eps(kx, ky):
    z =  np.exp((kx*d1[0]+ky*d1[1])*-1j) + np.exp((kx*d2[0]+ky*d2[1])*-1j) + np.exp((kx*d3[0]+ky*d3[1])*-1j)
    return np.sqrt((np.conj(z)*z).real)

N=200
x = np.linspace(-np.pi, np.pi, N)
X, Y = np.meshgrid(x,x)

Z = eps(X,Y)

ax = plt.axes(projection='3d')

ax.plot_surface(X,Y,Z,cmap='coolwarm')
ax.plot_surface(X,Y,-Z,cmap='coolwarm')
plt.show()

