from pyqcm import *

new_cluster_model('clus', 2, 0)
add_cluster('clus', [0, 0, 0], [[0, 0, 0], [1, 0, 0]])
lattice_model('h2', [[2, -2, 0],[2, 4, 0]], [[1, -1, 0], [2, 1, 0]])
set_basis([[1, 0, 0], [-0.5, 0.866025403784438, 0], [0, 0, 1]])

interaction_operator('U')
hopping_operator('t', [1,0,0], -1, band1=2, band2=1)
hopping_operator('t', [0,1,0], -1, band1=2, band2=1)
hopping_operator('t', [-1,-1,0], -1, band1=2, band2=1)

