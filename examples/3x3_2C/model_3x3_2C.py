from pyqcm import *

new_cluster_model('cluster', 9, 0)
add_cluster('cluster', [0, 0, 0], [
    [0, 0, 0],
    [1, 0, 0],
    [2, 0, 0],
    [0, 1, 0],
    [1, 1, 0],
    [2, 1, 0],
    [0, 2, 0],
    [1, 2, 0],
    [2, 2, 0]
])
add_cluster('cluster', [3, 0, 0], [
    [0, 0, 0],
    [1, 0, 0],
    [2, 0, 0],
    [0, 1, 0],
    [1, 1, 0],
    [2, 1, 0],
    [0, 2, 0],
    [1, 2, 0],
    [2, 2, 0]
])
lattice_model('3x3-2C', [[6, 0, 0], [1, 3, 0]])

interaction_operator('U')
hopping_operator('t', [1, 0, 0], -1)
hopping_operator('t', [0, 1, 0], -1)
hopping_operator('tm', [0, 1, 0], -1, sigma=3)
hopping_operator('tm', [1, 0, 0], -1, sigma=3)
anomalous_operator('S', [0, 0, 0], 1.0)
anomalous_operator('D', [1, 0, 0], 1.0)
anomalous_operator('D', [0, 1, 0], -1.0)
anomalous_operator('pip', [1, 0, 0], 1, type='dz')
anomalous_operator('pip', [0, 1, 0], 1j, type='dz')
anomalous_operator('pXx', [1, 0, 0], 1, type='dx')
density_wave('M', 'Z', [1, 1, 0])
density_wave('Mx', 'X', [1, 1, 0])
