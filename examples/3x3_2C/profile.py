from pyqcm import *
from pyqcm.profile import *
import model_3x3_2C

set_target_sectors(['R0', 'R0'])
set_parameters("""
mu = 0.5
U = 0.001
t = 1
M = 0.5
Mx = 0.3
pXx = 0.2
""")
new_model_instance()
plot_profile(bond_spin_scale=1, spin_angle=0, n_scale=0.5, singlet_scale=10, triplet_scale=4)
