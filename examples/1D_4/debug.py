from pyqcm import *
from pyqcm.spectral import *
set_global_parameter('nosym')
set_global_parameter('periodic')
import model_1D_4

set_global_parameter("verbose", 7)
# set_global_parameter("factorized")
# set_global_parameter("print_Hamiltonian")
# sec = 'R0:N4:S0'
sec = 'R0:N4:S0'
set_target_sectors([sec])
set_parameters("""
t=1
U = 2
mu = 1
""")
 
# I = new_model_instance(record=True)
# I.print('record.py')
# cluster_spectral_function()
# print_cluster_averages(cluster_averages())

new_model_instance()
# wietek_solver(sec, niter=20, label=0)
# cluster_spectral_function()

# spectral_function(wmax = 6, nk = 32, path='line')
# cluster_spectral_function(wmax = 4)
#qcm.averages()
print(ground_state())