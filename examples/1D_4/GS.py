import numpy as np
from pyqcm import *
import model_1D_4


set_target_sectors(['R0:S0'])
set_parameters("""
t = 1
mu = 0
D = 0.0000001
""")
for mu in np.arange(-0.1,0.1,0.01):
    set_parameter('mu', mu)
    new_model_instance()
    print('\nmu = ', mu)
    print_averages(cluster_averages())

