from pyqcm import *
from pyqcm.spectral import *
import model_1D_4

set_global_parameter('max_dim_full', 300)
set_target_sectors(['R0:N4:S0'])
set_parameters("""
U=4
mu=0.5*U
t=1
""")

new_model_instance()

spectral_function_Lehmann(path='halfline', offset=0.02, nk=64, band=1, lims=[-3,3])


k = np.array([[0.25, 0, 0]])
print(gap(k)) 