from pyqcm import *
from pyqcm.spectral import *
import model_1D_4

# set_global_parameter("print_Hamiltonian")
# set_global_parameter('max_dim_full',600)
set_target_sectors(['R0:N4:S0'])
set_parameters("""
U=4
mu=0.5*U
t=1
""")
 

I = new_model_instance(record=True)
I.print('record.py')
# cluster_spectral_function(5, full=True, offset=6)
spectral_function(5, eta = 0.002, path='line', nk=32)
# dispersion()