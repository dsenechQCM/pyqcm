from pyqcm import *
from pyqcm.spectral import *
import qcm_ED
from pyqcm.wietek import wietek_solver
import timeit

# set_global_parameter('periodic')
set_global_parameter('nosym')
import model_1D_2_4b

# set_global_parameter('max_dim_full', 500)
set_global_parameter('verbose', 5)
# set_global_parameter('accur_lanczos', 1e-15)
set_global_parameter('accur_band_lanczos', 1e-14)
cf = False
# set_global_parameter('GF_solver', 'cf'); cf = True

sec = 'R0:N6:S0'
set_target_sectors([sec])

set_parameters("""
t = 1, 
U=4
mu = 0.5 * U
tb1_1 = 0.5
tb2_1 = -1*tb1_1
eb1_1 = 1
eb2_1 = -1*eb1_1
""")

t = timeit.default_timer()
I = new_model_instance(record=True, label=1)
I.print('record.py')
t = timeit.default_timer() - t
print('temps de calcul (qcm) : ', t)
w, A1 = cluster_spectral_function(8, full=True, label=1)
# spectral_function(path='line', label=1)

t = timeit.default_timer()
new_model_instance()
wietek_solver(sec, niter=400, dyniter=200, cf=cf)
t = timeit.default_timer() - t
print('temps de calcul (qcm) : ', t)
w, A2 = cluster_spectral_function(8, full=True)
# spectral_function(path='line')

plot_spectral_array(w, A1-A2, offset=0.5)