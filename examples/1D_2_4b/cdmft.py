from pyqcm import *
from pyqcm.cdmft import cdmft
from pyqcm.loop import *

import model_1D_2_4b

np.set_printoptions(precision=4, linewidth=512, suppress=True)

set_global_parameter('print_all')
set_global_parameter('verbose',0)
set_target_sectors(['R0:N6:S0'])
set_parameters("""
    U=4
    mu=0.5*U
    t=1
    tp = 1e-8
    tb1_1=0.5
    tb2_1 = 0.5
    eb1_1=1
    eb2_1=-1
""")

varia=['tb1_1', 'tb2_1', 'eb1_1', 'eb2_1']

def F():
    cdmft(varia=varia, accur=1e-4, accur_hybrid=1e-6)

def F2():
    new_model_instance()
    averages()

# controlled_loop(
#     func=F,
#     varia=varia, 
#     loop_param='mu', 
#     loop_range=(2.0, 0.0, -0.02), 
#     control_func=None
# )


loop_from_file(F2, 'cdmft.csv')