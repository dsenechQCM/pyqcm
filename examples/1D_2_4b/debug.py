from pyqcm import *
from pyqcm.cdmft import *

import model_1D_2_4b

np.set_printoptions(precision=4, linewidth=512, suppress=True)

sec = 'R0:S0'
set_global_parameter('verbose',0)
set_target_sectors([sec])
pset = """
    U=4
    mu=1
    t=1
    D = 1e-8
"""

# set_params_from_file('cdmft.csv', n=1)

# from pyqcm.wietek import *
# I = new_model_instance(record=True)
# I.print('record.py')

# spectral_function()
varia = ['tb1_1', 'tb2_1', 'eb1_1', 'eb2_1', 'sb1_1', 'sb2_1']

# print(pset)


seq = ["""
tb1_1=0.5
tb2_1=0.5
eb1_1=1
eb2_1=-1
""",
"""
sb1_1=0.1
sb2_1=0.2
"""
]
set_parameters(pset+seq[0]+seq[1])

cdmft_forcing('D', [0.2, 0.1, 0.05, 0.02, 0.01, 0.001, 0.0001], beta_seq=[50]*7, varia=varia)

# cdmft_variational_sequence(pset, seq, method='COBYLA', initial_step = 0.02)