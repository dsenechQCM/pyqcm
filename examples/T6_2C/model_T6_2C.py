from pyqcm import *

new_cluster_model('T6', 6, 0)
add_cluster('T6', [0, 0, 0], [
    [0, 0, 0],
    [1, 0, 0],
    [2, 0, 0],
    [1, 1, 0],
    [2, 1, 0],
    [2, 2, 0]
])
add_cluster('T6', [5, 2, 0], [
    [0, 0, 0],
    [-1, 0, 0],
    [-2, 0, 0],
    [-1, -1, 0],
    [-2, -1, 0],
    [-2, -2, 0]
])


lattice_model('T6_2C', [[-2, 2, 0], [3, 3, 0]])
set_basis([
    [1, 0, 0],
    [-0.5, 0.866025403784438, 0],
    [0, 0, 1]
])

interaction_operator('U')
hopping_operator('t', [1, 0, 0], -1)  # NN hopping
hopping_operator('t', [0, 1, 0], -1)  # NN hopping
hopping_operator('t', [1, 1, 0], -1)  # NN hopping
density_wave('M', 'Z', [1.333333333, 0, 0])
density_wave('M', 'X', [1.333333333, 0, 0], phase=0.5)
print_model('model.out')
