from pyqcm import *
from pyqcm.spectral import *

import model_T6_2C

set_target_sectors(['R0:N6:S0', 'R0:N6:S0'])
set_parameters("""
t = 1
U = 2
mu = -1
""")
new_model_instance()

spectral_function(5, path='graphene', nk=32)
