from pyqcm import *
from pyqcm.profile import *
import model_T6_2C

U = 0.0
set_target_sectors(['R0:N6', 'R0:N6'])
set_parameters("""
t = 1
M = 0
M_1 = 0.5
M_2 = 1*M_1
""")
new_model_instance()
plot_profile(bond_scale=1, spin_scale=3, bond_spin_scale=1, spin_angle=0, n_scale=1, singlet_scale=1, triplet_scale=1)
