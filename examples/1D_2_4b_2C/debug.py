from pyqcm import *
from pyqcm.cdmft import cdmft
import model_1D_2_4b_2C

np.set_printoptions(precision=4, linewidth=512, suppress=True)
 
set_target_sectors(['R0:N6:S0','R0:N6:S0'])
set_parameters("""
    U=4
    mu=1
    t=1
    tp=0.2
    tb1_1=0.5
    tb2_1=0.5
    eb1_1=1
    eb2_1=-1
    tb1_2=0.5
    tb2_2=0.5
    eb1_2=1
    eb2_2=-1
""")

from pyqcm.spectral import *
new_model_instance()

from pyqcm.spectral import *
w = np.array([x + 0.05j*np.abs(x) for x in np.arange(-8,8,0.01)])
DoS(w)

# cdmft(varia=['tb1_1', 'tb2_1', 'eb1_1', 'eb2_1', 'tb1_2', 'tb2_2', 'eb1_2', 'eb2_2'], accur=1e-5)
# cdmft(varia=['tb1_1', 'tb2_1', 'eb1_1', 'eb2_1'], accur=1e-5)
# I = new_model_instance(record=True)
# I.print()
# print_cluster_averages(cluster_averages())
