from pyqcm import *

no = 10
ns = 4
new_cluster_model('clus', ns, no-ns, [[1, 3, 4, 2, 6, 7, 5, 9, 10, 8]])


new_cluster_operator('clus', 'bt1', 'one-body', [
    (2, 5, 1.0),
    (3, 6, 1.0),
    (4, 7, 1.0),
    (2+no, 5+no, 1.0),
    (3+no, 6+no, 1.0),
    (4+no, 7+no, 1.0)
])

new_cluster_operator('clus', 'bt2', 'one-body', [
    (2, 8, 1.0),
    (3, 9, 1.0),
    (4, 10, 1.0),
    (2+no, 8+no, 1.0),
    (3+no, 9+no, 1.0),
    (4+no, 10+no, 1.0)
])

new_cluster_operator('clus', 'be1', 'one-body', [
    (5, 5, 1.0),
    (6, 6, 1.0),
    (7, 7, 1.0),
    (5+no, 5+no, 1.0),
    (6+no, 6+no, 1.0),
    (7+no, 7+no, 1.0)
])

new_cluster_operator('clus', 'be2', 'one-body', [
    (8, 8, 1.0),
    (9, 9, 1.0),
    (10, 10, 1.0),
    (8+no, 8+no, 1.0),
    (9+no, 9+no, 1.0),
    (10+no, 10+no, 1.0)
])



#-------------------------------------------------------------------
# construction of the lattice model 

add_cluster('clus', [-1, 0, 0], [[0, 0, 0], [-1, 0, 0], [1, 1, 0], [0, -1, 0]])
add_cluster('clus', [1, 0, 0], [[0, 0, 0], [1, 0, 0], [-1, -1, 0], [0, 1, 0]])
lattice_model('h4_6b_C3', [[2, -2, 0],[2, 4, 0]], [[1, -1, 0], [2, 1, 0]])
set_basis([[1, 0, 0], [-0.5, 0.866025403784438, 0], [0, 0, 1]])

interaction_operator('U')
hopping_operator('t', [1,0,0], -1, band1=2, band2=1)
hopping_operator('t', [0,1,0], -1, band1=2, band2=1)
hopping_operator('t', [-1,-1,0], -1, band1=2, band2=1)

# print_model('model.out', asy_operators=True)