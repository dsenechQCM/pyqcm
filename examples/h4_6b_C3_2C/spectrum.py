from pyqcm import *
from pyqcm.spectral import *
import model_h4_6b_C3_2C

set_global_parameter('verbose', 0)
set_target_sectors(['R0:N10:S0/R1:N10:S0/R2:N10:S0', 'R0:N10:S0/R1:N10:S0/R2:N10:S0'])
set_parameters("""
t = 1,
U = 4,
mu = 0.5*U,
bt1_1 =  1,
bt2_1 =  -1,
be1_1 =  0.5,
be2_1 =  -0.4,
bt1_2 =  1*bt1_1,
bt2_2 =  1*bt2_1,
be1_2 =  1*be1_1,
be2_2 =  1*be2_1,
""")
new_model_instance()
print(ground_state())
spectral_function(wmax=1, path='graphene', nk=1)
