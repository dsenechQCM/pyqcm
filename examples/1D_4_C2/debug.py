from pyqcm import *
from pyqcm.spectral import *
import model_1D_4_C2

set_global_parameter("print_Hamiltonian")
set_global_parameter("max_dim_full", 64)
set_target_sectors(['R0:S0'])
set_parameters("""
U=4
mu=2
t=1
S=0.5
""")
 
for R in ['R0:S0', 'R1:S0', 'R0:S1', 'R1:S1']:
    set_target_sectors([R])
    new_model_instance()
    print(ground_state())
