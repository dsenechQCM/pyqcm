from pyqcm import *
from pyqcm.spectral import *
import model_1D_4_C2

print_model('model.out')
set_global_parameter("print_Hamiltonian")
set_global_parameter("max_dim_full", 64)
set_target_sectors(['R0:S0'])
set_parameters("""
t = 1
Si = 0.1
U = 0.00001
""")
qcm.new_model_instance()
spectral_function(wmax=5, path='line', nk=32)
