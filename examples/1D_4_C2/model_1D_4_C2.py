from pyqcm import *

new_cluster_model('L4', 4, 0, [[4, 3, 2, 1]])
add_cluster('L4', [0, 0, 0], [[0, 0, 0], [1, 0, 0], [2, 0, 0], [3, 0, 0]])
lattice_model('1D_L4', [[4, 0, 0]])

interaction_operator('U')

hopping_operator('t', [1, 0, 0], -1)  # NN hopping
hopping_operator('tp', [2, 0, 0], -1)  # NNN hopping
hopping_operator('sf', [0, 0, 0], -1, tau=0, sigma=1)  # on-site spin flip
anomalous_operator('D', [1, 0, 0], 1)  # NN singlet
anomalous_operator('Di', [1, 0, 0], 1j)  # NN singlet with imaginary amplitude
anomalous_operator('S', [0, 0, 0], 1)  # on-site singlet
anomalous_operator('Si', [0, 0, 0], 1j)  # on-site singlet with imaginary amplitude
